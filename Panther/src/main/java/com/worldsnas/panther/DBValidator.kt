package com.worldsnas.panther

interface DBValidator<Key, DBModel> {

    fun isEmpty(key: Key, dbModel: DBModel) : Boolean
}