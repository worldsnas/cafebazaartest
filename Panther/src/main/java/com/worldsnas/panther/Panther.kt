package com.worldsnas.panther

import io.reactivex.Observable
import io.reactivex.Single

interface Panther<RepoModel, ServerModel, DBModel, Key> {

    fun fetch(key: Key) : Single<RepoModel>

    fun fetchAndSave(key: Key) : Single<RepoModel>

    fun observe(key: Key) : Observable<RepoModel>

    fun observeGet(key: Key) : Observable<RepoModel>

    fun get(key: Key) : Single<RepoModel>

    fun read(key: Key) : Single<RepoModel>

    fun clear(key: Key) : Single<Boolean>

    fun clearWithModel(repoModel: RepoModel) : Single<Boolean>
}