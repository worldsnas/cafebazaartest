package com.worldsnas.panther

import io.reactivex.Observable
import io.reactivex.Single

interface Persister<Key, DBModel> {

    fun read(key: Key) : Single<DBModel>

    fun write(key: Key, dbModel: DBModel) : Single<Boolean>

    fun observe(key: Key) : Observable<DBModel>

    fun clear(key: Key) : Single<Boolean>

    fun clearWithModel(dbModel: DBModel) : Single<Boolean>
}