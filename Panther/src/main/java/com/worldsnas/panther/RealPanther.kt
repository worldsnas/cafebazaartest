package com.worldsnas.panther


import io.reactivex.Observable
import io.reactivex.Single

class RealPanther<RepoModel, ServerModel, DBModel, Key>(private val fetcher: Fetcher<Key, ServerModel>,
                                                        private val persister: Persister<Key, DBModel>,
                                                        private val serverMapper: ServerMapper<ServerModel, DBModel>,
                                                        private val dBMapper: DBMapper<DBModel, RepoModel>,
                                                        private val dbValidator: DBValidator<Key, DBModel>)
    : Panther<RepoModel, ServerModel, DBModel, Key> {


    override fun fetch(key: Key): Single<RepoModel> = fetcher.fetch(key)
            .map {
                val dbModel = serverMapper.map(it)
                dBMapper.mapDb(dbModel)
            }

    override fun fetchAndSave(key: Key): Single<RepoModel> = fetcher.fetch(key)
            .map {
                serverMapper.map(it)
            }
            .doOnSuccess {
                persister.write(key, it).toObservable().publish().autoConnect(0)
            }
            .map {
                dBMapper.mapDb(it)
            }

    override fun observe(key: Key): Observable<RepoModel> =
            persister.observe(key)
                    .map {
                        dBMapper.mapDb(it)
                    }

    override fun observeGet(key: Key): Observable<RepoModel> =
            persister.read(key)
                    .map {
                        dBMapper.mapDb(it)
                    }
                    .toObservable()
                    .concatWith(fetchAndSave(key).toObservable())
                    .concatWith(persister.observe(key)
                            .skip(1)
                            .map {
                                dBMapper.mapDb(it)
                            })


    override fun get(key: Key): Single<RepoModel> =
            persister.read(key)
                    .flatMap {
                        if (dbValidator.isEmpty(key, it)) {
                            fetchAndSave(key)
                        } else {
                            Single.just(dBMapper.mapDb(it))
                        }
                    }

    override fun read(key: Key): Single<RepoModel> = persister.read(key)
            .map { dBMapper.mapDb(it) }

    override fun clear(key: Key): Single<Boolean> =
            Single.just(key)
                    .flatMap {
                        persister.clear(it)
                    }

    override fun clearWithModel(repoModel: RepoModel): Single<Boolean> =
            Single.just(repoModel)
                    .map {
                        dBMapper.mapRepo(it)
                    }
                    .flatMap {
                        persister.clearWithModel(it)
                    }

    class Builder<RepoModel, ServerModel, DBModel, Key> {
        private var fetcher: Fetcher<Key, ServerModel>? = null
        private var persister: Persister<Key, DBModel>? = null
        private var serverMapper: ServerMapper<ServerModel, DBModel>? = null
        private var dBMapper: DBMapper<DBModel, RepoModel>? = null
        private var dbValidator: DBValidator<Key, DBModel>? = null

        fun setFetcher(fetcher: Fetcher<Key, ServerModel>): Builder<RepoModel, ServerModel, DBModel, Key> {
            this.fetcher = fetcher
            return this
        }

        fun setPersister(persister: Persister<Key, DBModel>): Builder<RepoModel, ServerModel, DBModel, Key> {
            this.persister = persister
            return this
        }

        fun setServerMapper(serverMapper: ServerMapper<ServerModel, DBModel>): Builder<RepoModel, ServerModel, DBModel, Key> {
            this.serverMapper = serverMapper
            return this
        }

        fun setDBMapper(dBMapper: DBMapper<DBModel, RepoModel>): Builder<RepoModel, ServerModel, DBModel, Key> {
            this.dBMapper = dBMapper
            return this
        }

        fun setDBValidator(dbValidator: DBValidator<Key, DBModel>): Builder<RepoModel, ServerModel, DBModel, Key> {
            this.dbValidator = dbValidator
            return this
        }

        fun build(): Panther<RepoModel, ServerModel, DBModel, Key> =
                RealPanther(fetcher!!, persister!!, serverMapper!!, dBMapper!!, dbValidator!!)
    }
}