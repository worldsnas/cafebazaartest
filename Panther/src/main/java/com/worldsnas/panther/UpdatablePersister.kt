package com.worldsnas.panther

import io.reactivex.Single


abstract class UpdatablePersister<Key, DBModel>(val updatePolicy: UpdatePolicy<Key, DBModel>?) : Persister<Key, DBModel> {
    final override fun write(key: Key, dbModel: DBModel): Single<Boolean> =
            if (updatePolicy == null) {
                writeWithUpdatePolicy(key, dbModel)
            } else
                writeWithUpdatePolicy(key, updatePolicy.update(dbModel, read(key).blockingGet()))

    abstract fun writeWithUpdatePolicy(key: Key, dbModel: DBModel): Single<Boolean>
}