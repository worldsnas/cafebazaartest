package com.worldsnas.panther

open class Result<Data, Error> (val data : Data?,
                                val error : Error?)