package com.worldsnas.panther

interface DBMapper<DBModel, RepoModel> {

    fun mapDb(dbModel: DBModel) : RepoModel

    fun mapRepo(repoModel: RepoModel) : DBModel
}