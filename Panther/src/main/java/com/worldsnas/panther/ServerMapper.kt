package com.worldsnas.panther

interface ServerMapper<ServerModel, DBModel> {

    fun map(serverModel: ServerModel) : DBModel
}