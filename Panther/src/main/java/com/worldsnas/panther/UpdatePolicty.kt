package com.worldsnas.panther

interface UpdatePolicy<Key, DBModel>{
     fun update(newItems:  DBModel , persisterItems: DBModel): DBModel
}