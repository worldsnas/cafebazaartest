package com.worldsnas.panther

import io.reactivex.Single

interface Fetcher<Key, ServerModel> {

    fun fetch(key: Key) : Single<ServerModel>
}