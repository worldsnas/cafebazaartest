package com.worldsnas.cafebazaartest.activity.detail.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.runner.AndroidJUnit4
import com.nhaarman.mockitokotlin2.mock
import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.detail.presentation.DetailActivityViewModel
import com.worldsnas.cafebazaartest.activity.detail.uimodels.DetailActivityIntent
import com.worldsnas.cafebazaartest.activity.detail.uimodels.DetailActivityState
import com.worldsnas.cafebazaartest.app.di.Injector
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import util.factory.createRandomVenue
import util.randomBoolean
import util.randomString
import util.rule.RxTestSchedulerRule

@RunWith(AndroidJUnit4::class)
class DetailActivityTest{

    private val viewModel = mock<DetailActivityViewModel>()

    @get:Rule
    val scheduler = RxTestSchedulerRule()

    @get:Rule
    val activityRule = Injector.createBaseActivityRule<DetailActivityIntent, DetailActivityState, DetailActivityViewModel, DetailActivity>(viewModel, true, true)

    @Test
    fun showCorrectNameTest(){
        val venue = createRandomVenue()
        activityRule.activity.render(DetailActivityState(randomBoolean(),
                emptyMap(),
                venue,
                randomBoolean(),
                randomString(),
                false,
                null,
                randomBoolean(),
                randomBoolean(),
                R.string.error_server))

        onView(withText(venue.name)).check(matches(isDisplayed()))

    }
}