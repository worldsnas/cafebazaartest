package com.worldsnas.cafebazaartest.activity.main

import androidx.test.runner.AndroidJUnit4
import com.nhaarman.mockitokotlin2.mock
import com.worldsnas.cafebazaartest.activity.detail.presentation.DetailActivityViewModel
import com.worldsnas.cafebazaartest.app.di.Injector
import org.junit.Rule
import org.junit.runner.RunWith
import util.rule.RxTestSchedulerRule

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    private val viewModel = mock<DetailActivityViewModel>()

    @get:Rule
    val scheduler = RxTestSchedulerRule()

    @get:Rule
    val activityRule = Injector.createBaseActivityRule(viewModel, true, false)


}