package com.worldsnas.cafebazaartest.app.di

import android.app.Activity
import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.test.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.worldsnas.cafebazaartest.app.App
import com.worldsnas.cafebazaartest.base.BaseActivity
import com.worldsnas.cafebazaartest.base.BaseViewModel
import com.worldsnas.cafebazaartest.mvibase.MviIntent
import com.worldsnas.cafebazaartest.mvibase.MviViewState
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.DispatchingAndroidInjector_Factory
import javax.inject.Provider
import kotlin.Function

internal class Injector<T> {
    private val providerMap: MutableMap<Class<out T>, Provider<AndroidInjector.Factory<out T>>> = mutableMapOf()
    private val dispatchingAndroidInjector: DispatchingAndroidInjector<T> = DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(providerMap)

    inline fun <reified A : Application> injectApplication(
            crossinline initBlock: A.(injector: DispatchingAndroidInjector<T>) -> Unit
    ) {
        (InstrumentationRegistry.getTargetContext().applicationContext as? A)?.apply {
            initBlock(dispatchingAndroidInjector)
        }
    }

    inline fun <reified F : T> registerInjector(crossinline initBlock: F.() -> Unit) : F?{
        var act : F? = null
        val injector = AndroidInjector<F> { fragment ->
            fragment.initBlock()
            act = fragment
        }
        val factory: AndroidInjector.Factory<out T> = AndroidInjector.Factory<F> { injector }
        providerMap[F::class.java] = Provider { factory }
        return act
    }

    companion object {
        inline fun <I : MviIntent, S : MviViewState, reified V : BaseViewModel<I, S>, reified A : BaseActivity<I, S, V>>
                createBaseActivityRule(viewModel: V, initialTouchMode: Boolean, launchActivity: Boolean,noinline callback : A.()->Unit = {}): ActivityTestRule<A> {
            val injector = Injector<Activity>()
            return object : ActivityTestRule<A>(A::class.java, initialTouchMode, launchActivity) {
                override fun beforeActivityLaunched() {
                    super.beforeActivityLaunched()
                    injector.apply {
                        injectApplication<App> { activityInjector ->
                            this.activityInjector = activityInjector
                        }
                        registerBaseActivityInjector<I, S, V>(viewModel)
                    }
                    (activity).callback()
                }
            }
        }
    }
}

internal fun <I : MviIntent, S : MviViewState, V : BaseViewModel<I, S>>
        Injector<Activity>.registerBaseActivityInjector(viewModel: ViewModel) =
        registerInjector<BaseActivity<I, S, V>> {
            injectViewModelFactory(createViewModelFactory(viewModel))
        }

inline fun <reified T : ViewModel> createViewModelFactory(viewModel: T): ViewModelProvider.Factory =
        mock<ViewModelProvider.Factory>().apply {
            whenever(create(T::class.java)).thenReturn(viewModel)
        }