package util

import java.util.*

val rand = Random()

fun randomString() = UUID.randomUUID().toString()

fun randomBoolean() = rand.nextBoolean()

fun randomInt() = rand.nextInt()

fun randomLong() = rand.nextLong()