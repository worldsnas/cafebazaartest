package util.factory

import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import util.randomBoolean
import util.randomInt
import util.randomLong
import util.randomString

fun createRandomVenue() = UiVenue(randomLong(), randomString(), randomString(), randomString(), randomString(), randomString(), randomBoolean(), randomInt())