package util.factory

import com.worldsnas.cafebazaartest.entity.DetailVenueEntity
import util.randomInt
import util.randomLong
import util.randomString

fun createDetailVenue() =
        DetailVenueEntity(
                randomInt(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomString(),
                randomLong()
        )