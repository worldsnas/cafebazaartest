package util.factory

import com.worldsnas.cafebazaartest.entity.DetailVenuePhotoEntity
import util.randomLong
import util.randomString

fun createPhotoEntity() =
        DetailVenuePhotoEntity(
                randomLong(),
                randomLong(),
                randomString(),
                randomString()
        )