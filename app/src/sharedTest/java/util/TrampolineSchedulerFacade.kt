package util

import com.worldsnas.cafebazaartest.rxutils.SchedulersFacade
import io.reactivex.Scheduler
import util.rule.TRAMPOLINE_SCHEDULER_INSTANCE

object TrampolineSchedulerFacade : SchedulersFacade {
    override fun io(): Scheduler =
             TRAMPOLINE_SCHEDULER_INSTANCE
    override fun computation(): Scheduler =
            TRAMPOLINE_SCHEDULER_INSTANCE
    override fun single(): Scheduler =
            TRAMPOLINE_SCHEDULER_INSTANCE
    override fun ui(): Scheduler =
            TRAMPOLINE_SCHEDULER_INSTANCE
}