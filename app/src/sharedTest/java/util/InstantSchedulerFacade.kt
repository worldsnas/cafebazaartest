package util

import util.rule.TEST_SCHEDULER_INSTANCE
import com.worldsnas.cafebazaartest.rxutils.SchedulersFacade
import io.reactivex.Scheduler

object InstantSchedulerFacade : SchedulersFacade{
    override fun io(): Scheduler =
            TEST_SCHEDULER_INSTANCE
    override fun computation(): Scheduler =
            TEST_SCHEDULER_INSTANCE
    override fun single(): Scheduler =
            TEST_SCHEDULER_INSTANCE
    override fun ui(): Scheduler =
            TEST_SCHEDULER_INSTANCE
}