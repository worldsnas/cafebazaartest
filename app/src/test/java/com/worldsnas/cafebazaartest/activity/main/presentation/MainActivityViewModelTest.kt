package com.worldsnas.cafebazaartest.activity.main.presentation

import com.nhaarman.mockitokotlin2.mock
import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityAction
import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityResult
import com.worldsnas.cafebazaartest.mvibase.MviProcessor
import org.junit.Rule
import org.junit.Test
import util.rule.RxTrampolineSchedulerRule

class MainActivityViewModelTest{

    private val processor = mock<MviProcessor<MainActivityAction, MainActivityResult>>()

    private val viewModel = MainActivityViewModel(processor)

    @get:Rule
    val scheduler = RxTrampolineSchedulerRule()

    @Test
    fun testInitialIntent(){
        viewModel
    }
}