package com.worldsnas.cafebazaartest.activity.main.model

import com.nhaarman.mockitokotlin2.mock
import com.worldsnas.cafebazaartest.activity.main.model.repo.MainActivityRepoModel
import com.worldsnas.cafebazaartest.activity.main.model.repo.MainRepoKey
import com.worldsnas.cafebazaartest.activity.main.model.repo.db.MainActivityDBModel
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.search.RecommendationResponseModel
import com.worldsnas.panther.Panther
import org.junit.Rule
import org.junit.Test
import retrofit2.Response
import util.InstantSchedulerFacade
import util.rule.RxTrampolineSchedulerRule

class MainActivityProcessorHolderTest {

    private val repo = mock<Panther<MainActivityRepoModel, Response<BaseModel<RecommendationResponseModel>>, MainActivityDBModel, MainRepoKey>>()

    val processor = MainActivityProcessorHolder(repo, InstantSchedulerFacade)

    @get:Rule
    val scheduler = RxTrampolineSchedulerRule()

    @Test
    fun initialActionTest() {
        processor
    }
}