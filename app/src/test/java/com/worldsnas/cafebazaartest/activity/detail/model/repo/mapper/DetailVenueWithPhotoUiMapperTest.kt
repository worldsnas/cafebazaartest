package com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper

import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import util.factory.createDetailVenue
import util.factory.createPhotoEntity

class DetailVenueWithPhotoUiMapperTest{

    private val mapper = DetailVenueWithPhotoUiMapper()

    @Test
    fun detailVenueMapsToUiTest(){
        val detailVenue = createDetailVenue()
        val photos = listOf(createPhotoEntity())
        val detail = DetailVenueWithPhoto(detailVenue, photos)

        val ui = mapper.mapTo(detail)

        assertThat(ui.address, `is`(equalTo(detailVenue.address)))
        assertThat(ui.bestPhoto, `is`(equalTo(detailVenue.bestPhoto)))
        assertThat(ui.cat, `is`(equalTo(detailVenue.cat)))
        assertThat(ui.catIcon, `is`(equalTo(detailVenue.catIcon)))
        assertThat(ui.rate, `is`(equalTo(detailVenue.rate)))
        assertThat(ui.showBestPhoto, `is`(equalTo(true)))
        assertThat(ui.venueId, `is`(equalTo(detailVenue.venue_id)))
        assertThat(ui.city, `is`(equalTo(detailVenue.city)))
        assertThat(ui.name, `is`(equalTo(detailVenue.name)))
        assertThat(ui.phone, `is`(equalTo(detailVenue.phone)))

    }

}