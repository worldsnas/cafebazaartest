package com.worldsnas.cafebazaartest.activity.detail.presentation

import com.nhaarman.mockitokotlin2.mock
import com.worldsnas.cafebazaartest.activity.detail.model.DetailActivityProcessorHolder
import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailActivityRepoModel
import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailRepoKey
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailActivityDBModel
import com.worldsnas.cafebazaartest.activity.detail.uimodels.DetailActivityIntent
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailResponseModel
import com.worldsnas.panther.Panther
import io.reactivex.Observable
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test
import retrofit2.Response
import timber.log.Timber
import util.TrampolineSchedulerFacade
import util.factory.createRandomVenue
import util.rule.RxTrampolineSchedulerRule
import java.util.*

class DetailActivityViewModelTest {

    private val panther = mock<Panther<DetailActivityRepoModel, Response<BaseModel<VenueDetailResponseModel>>, DetailActivityDBModel, DetailRepoKey>>()

    private val processor = DetailActivityProcessorHolder(panther, TrampolineSchedulerFacade)

    private val viewModel = DetailActivityViewModel(processor)

    @get:Rule
    val instantRxScheduler = RxTrampolineSchedulerRule()

    @Test
    fun initialIntentReturnsCorrectStateTest() {
        val uiVenue = createRandomVenue()

        viewModel.processIntents(Observable.just(DetailActivityIntent.InitialIntent(uiVenue)))

        val observer = viewModel.states()
                .test()

        observer.assertNotComplete()
        observer.assertValueCount(1)
        val state = observer.values()[0]


        assertThat(false, `is`(equalTo(state.closeActivity)))
        assertThat(true, `is`(equalTo(state.loading)))
        assertThat(false, `is`(equalTo(state.showError)))
        assertNull(state.detailVenue)
        assertThat(uiVenue, `is`(equalTo(state.searchedVenue)))
    }
}