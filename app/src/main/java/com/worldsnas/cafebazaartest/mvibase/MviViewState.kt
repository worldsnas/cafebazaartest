package com.worldsnas.cafebazaartest.mvibase

/**
 * Immutable object which contains all the required information to render a [MviView].
 */
interface MviViewState
