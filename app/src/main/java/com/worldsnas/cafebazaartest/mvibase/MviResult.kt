package com.worldsnas.cafebazaartest.mvibase

/**
 * Immutable object resulting of a processed business logic.
 */
interface MviResult
