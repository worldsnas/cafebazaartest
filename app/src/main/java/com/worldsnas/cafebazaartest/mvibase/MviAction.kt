package com.worldsnas.cafebazaartest.mvibase

/**
 * Immutable object which contains all the required information for a business logic to process.
 */
interface MviAction
