package com.worldsnas.cafebazaartest.mvibase

/**
 * Immutable object which represent an view's intent.
 */
interface MviIntent
