package com.worldsnas.cafebazaartest.widgets

import android.content.Context
import android.util.AttributeSet

import com.worldsnas.cafebazaartest.R

import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.res.ResourcesCompat


/**
 * @author Mehrdadsml@gmail.com
 */

class Button @JvmOverloads constructor(context : Context, attr : AttributeSet? = null, defStyleAttr: Int = 0): AppCompatButton(context, attr, defStyleAttr){

    init{
        typeface = ResourcesCompat.getFont(context, R.font.iransansmobile_fanum)

    }
}
