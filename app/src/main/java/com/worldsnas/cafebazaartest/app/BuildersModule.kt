package com.worldsnas.cafebazaartest.app


import com.worldsnas.cafebazaartest.activity.detail.di.DetailActivityModule
import com.worldsnas.cafebazaartest.activity.detail.ui.DetailActivity
import com.worldsnas.cafebazaartest.activity.main.di.MainActivityModule
import com.worldsnas.cafebazaartest.activity.main.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class BuildersModule {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [DetailActivityModule::class])
    abstract fun bindDetailActivity(): DetailActivity

}
