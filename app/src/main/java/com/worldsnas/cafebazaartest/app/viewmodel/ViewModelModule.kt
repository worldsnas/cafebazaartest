package com.worldsnas.cafebazaartest.app.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.worldsnas.cafebazaartest.activity.detail.presentation.DetailActivityViewModelModule
import com.worldsnas.cafebazaartest.activity.main.presentation.MainActivityViewModelModule
import dagger.Binds
import dagger.Module

@Module(includes = [MainActivityViewModelModule::class,
    DetailActivityViewModelModule::class])
abstract class ViewModelModule{
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory) : ViewModelProvider.Factory

}
