package com.worldsnas.cafebazaartest.app


import com.worldsnas.cafebazaartest.app.network.NetworkModule
import com.worldsnas.cafebazaartest.app.pref.SharedPrefModule
import com.worldsnas.cafebazaartest.livedata.SingleLiveData
import com.worldsnas.cafebazaartest.manager.pref.AppPrefManager
import com.worldsnas.cafebazaartest.manager.pref.PrefManager
import com.worldsnas.cafebazaartest.rxutils.SchedulerFacadeImpl
import com.worldsnas.cafebazaartest.rxutils.SchedulersFacade

import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module(includes = arrayOf(NetworkModule::class, SharedPrefModule::class))
abstract class AppModule {

    @Binds
    internal abstract fun providePrefManager(prefManager: AppPrefManager): PrefManager

    @Binds
    internal abstract fun bindSchedulers(schedulerFacade: SchedulerFacadeImpl): SchedulersFacade

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideCompositeDisposable(): CompositeDisposable {
            return CompositeDisposable()
        }

        @JvmStatic
        @Provides
        fun provideIntegerSingleLiveData(): SingleLiveData<Int> {
            return SingleLiveData()
        }
    }

}
