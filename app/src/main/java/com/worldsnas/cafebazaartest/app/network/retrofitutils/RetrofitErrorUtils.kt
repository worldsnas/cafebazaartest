@file:JvmName("RetrofitErrorUtils")
package com.worldsnas.cafebazaartest.app.network.retrofitutils

import androidx.annotation.StringRes
import com.worldsnas.cafebazaartest.R
import com.squareup.moshi.JsonDataException
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.InterruptedIOException
import java.net.ConnectException
import java.util.concurrent.TimeoutException

fun getServerErrorStatusCode(throwable: Throwable): Int = when (throwable) {
    is JsonDataException -> STATUS_JSON_MALFORMED
    is TimeoutException -> STATUS_TIMEOUT
    is InterruptedIOException -> STATUS_INTERRUPTED
    is ConnectException -> STATUS_FAILED_CONNECT
    is NoSuchElementException -> STATUS_NO_SUCH_ELEMENT
    is IllegalArgumentException -> STATUS_ILLEGAL_ARGUMENT
    else -> STATUS_UNKNOWN
}

fun getServerErrorBody(throwable: Throwable): ResponseBody {
    val type = MediaType.parse("text")

    val message: String = when (throwable) {
        is JsonDataException -> "Malformed Json"
        is TimeoutException -> "TimeOut"
        is InterruptedIOException -> "Intrupted Connection"
        is ConnectException -> "Connection Failure"
        is NoSuchElementException -> "No Element Found"
        is IllegalArgumentException -> "Wrong Argument"
        else -> "Error"
    }

    return ResponseBody.create(type, message)
}

fun <T> createErrorResponse(throwable: Throwable): Response<T> {
    val status = getServerErrorStatusCode(throwable)
    val responseBody = getServerErrorBody(throwable)
    return Response.error(status, responseBody)
}

fun getThrowableErrorString(t : Throwable) = getStatusCodeErrorString(getServerErrorStatusCode(t))

@StringRes
fun getStatusCodeErrorString(code: Int) = when (code) {
    STATUS_NOT_FOUND -> R.string.error_not_found
    STATUS_BAD_REQUEST -> R.string.error_internal
    STATUS_SERVER_FAILURE -> R.string.error_server_failure
    STATUS_JSON_MALFORMED -> R.string.error_data_receive
    STATUS_TIMEOUT -> R.string.error_time_out
    STATUS_INTERRUPTED -> R.string.error_connection_failure
    STATUS_FAILED_CONNECT -> R.string.error_connection_failure
    STATUS_NO_SUCH_ELEMENT -> R.string.error_element_not_found
    STATUS_ILLEGAL_ARGUMENT -> R.string.error_illegal_argument
    STATUS_UNKNOWN -> R.string.error_unknown
    else -> R.string.error_server
}
