package com.worldsnas.cafebazaartest.app.db

import com.worldsnas.cafebazaartest.app.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import androidx.room.Room


@Module
object DBModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideBoxStore(app: App): AppDataBase {
        return Room.databaseBuilder(app,
                AppDataBase::class.java,
                "foursquare").build()
    }
}
