package com.worldsnas.cafebazaartest.app.pref

import android.content.SharedPreferences
import android.preference.PreferenceManager

import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.worldsnas.cafebazaartest.app.App

import dagger.Module
import dagger.Provides

@Module
object SharedPrefModule {

    @JvmStatic
    @Provides
    internal fun provideSharedPrefs(context: App): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @JvmStatic
    @Provides
    internal fun provideRXPreference(preferences: SharedPreferences): RxSharedPreferences {
        return RxSharedPreferences.create(preferences)
    }
}
