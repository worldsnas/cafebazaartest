package com.worldsnas.cafebazaartest.app.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailVenueDao
import com.worldsnas.cafebazaartest.activity.main.model.repo.db.SearchVenueDao
import com.worldsnas.cafebazaartest.entity.DetailVenueEntity
import com.worldsnas.cafebazaartest.entity.DetailVenuePhotoEntity
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity

@Database(entities = [SearchVenueEntity::class, DetailVenueEntity::class, DetailVenuePhotoEntity::class], version = 1)
abstract class AppDataBase :RoomDatabase() {

    abstract fun searchVenue(): SearchVenueDao

    abstract fun detailVenue(): DetailVenueDao
}