package com.worldsnas.cafebazaartest.app


import com.worldsnas.cafebazaartest.app.db.DBModule
import com.worldsnas.cafebazaartest.app.viewmodel.ViewModelModule

import javax.inject.Singleton

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, BuildersModule::class, DBModule::class, ViewModelModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>() {
        abstract override fun build(): AppComponent
    }

    override fun inject(app: App)
}
