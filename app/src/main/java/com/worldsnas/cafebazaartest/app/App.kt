package com.worldsnas.cafebazaartest.app

import android.app.Activity
import android.content.Context
import androidx.multidex.MultiDex
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import com.worldsnas.cafebazaartest.BuildConfig.DEBUG
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber
import javax.inject.Inject
import com.facebook.imagepipeline.core.ImagePipelineConfig



class App : DaggerApplication() {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        initMultiDex()
    }

    override fun onCreate() {
        super.onCreate()

        //can not be moved a initMethod (see decompiled class)
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        } else {
            LeakCanary.install(this)
        }
        Stetho.initializeWithDefaults(this)

        val imagePipelineConfig = ImagePipelineConfig.newBuilder(applicationContext)
                .setDownsampleEnabled(true)
                .build()
        Fresco.initialize(this, imagePipelineConfig)

        initTimber()
    }

    private fun initTimber() {
        if (DEBUG)
            Timber.plant(Timber.DebugTree())
    }

    private fun initMultiDex() {
        if (DEBUG)
            MultiDex.install(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val injector = DaggerAppComponent.builder().create(this)
        injector.inject(this)
        return injector
    }
    //endregion
}

