package com.worldsnas.cafebazaartest.app.network

import com.worldsnas.cafebazaartest.rxutils.SchedulersFacade
import com.worldsnas.cafebazaartest.servermodel.ApplicationJsonAdapterFactory
import com.squareup.moshi.Moshi

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module(includes = [OkHttpModule::class])
object NetworkModule {

    @JvmStatic
    @Provides
    fun provideTypeAdapter(): Moshi {
        val builder = Moshi.Builder()
        builder.add(ApplicationJsonAdapterFactory.INSTANCE)
        return builder.build()
    }


    @JvmStatic
    @Singleton
    @Provides
    fun provideRestHelper(client: OkHttpClient, moshi: Moshi, scheduler: SchedulersFacade): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(scheduler.io()))
                .build()
    }
}
