package com.worldsnas.cafebazaartest.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class SimpleItemDecorator(
        val top: Int = 0, val bottom: Int = 0, val left: Int = 0, val right: Int = 0
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.top = top
        outRect.bottom = bottom
        outRect.left = left
        outRect.right = right
//        super.getItemOffsets(outRect, view, parent, state)
    }

}