package com.worldsnas.cafebazaartest.utils

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.tabs.TabLayout
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.Retrofit
import java.math.RoundingMode
import java.text.DecimalFormat

inline fun <reified T> Retrofit.create(): T = create(T::class.java)


fun ViewGroup.inflate(resId: Int): View {
    return LayoutInflater.from(this.context).inflate(resId, this, false)
}

//inline fun <S : KBaseViewState, reified V : KBaseViewModel<S>> KBaseDaggerFragment<S, V>.createViewModel() {
//    createViewModel(V::class.java)
//}
//
//inline fun <S : KBaseViewState, reified V : KBaseViewModel<S>> KBaseDaggerCompatActivity<S, V>.createViewModel() {
//    createViewModel(V::class.java)
//}

fun Disposable.add(composite: CompositeDisposable): Disposable {
    composite.add(this)
    return this
}

fun TabLayout.changeTabsFont(font: Int) {
    val vg = getChildAt(0) as ViewGroup
    val tabsCount = vg.childCount
    for (j in 0 until tabsCount) {
        val vgTab = vg.getChildAt(j) as ViewGroup
        val tabChildsCount = vgTab.childCount
        for (i in 0 until tabChildsCount) {
            val tabViewChild = vgTab.getChildAt(i)
            if (tabViewChild is TextView) {
                tabViewChild.setTypeface(ResourcesCompat.getFont(this.context, font), Typeface.NORMAL)
            }
        }
    }
}

fun View.setVisibility(visible: Boolean) {
    visibility = if (visible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

fun Double.roundDecimal(decimal : Int) : Double{
    return "%.${decimal}f".format(this).toDouble()
//    val df = DecimalFormat("#.###")
//    df.roundingMode = RoundingMode.CEILING
//    return df.format(this).toDouble()
}
