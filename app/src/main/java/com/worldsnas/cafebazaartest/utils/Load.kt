package com.worldsnas.cafebazaartest.utils

data class Load(
        val page : Int,
        val totalCount : Int
){
    companion object {
        @JvmStatic
        fun create(page: Int, totalCount: Int) = Load(page, totalCount)
        @JvmStatic
        fun initial() = Load(0, 0)
    }
}