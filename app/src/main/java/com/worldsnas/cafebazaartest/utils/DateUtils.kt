@file:JvmName("DateUtils")
package com.worldsnas.cafebazaartest.utils

fun getYesterday() = System.currentTimeMillis() - (1000 * 60 * 60 * 24)