package com.worldsnas.cafebazaartest.servermodel.detail

import com.squareup.moshi.Json
import com.worldsnas.cafebazaartest.servermodel.search.PhotoModel
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class VenueDetailPhotoGroupModel(
        @Json(name = "count")
        val count: Long,
        @Json(name = "items")
        val photos: List<PhotoModel>
)