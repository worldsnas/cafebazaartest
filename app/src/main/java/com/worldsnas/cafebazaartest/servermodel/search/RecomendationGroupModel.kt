package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class RecomendationGroupModel (
        @Json(name= "results")
        val results : List<ResultModel>?)