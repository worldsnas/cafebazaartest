package com.worldsnas.cafebazaartest.servermodel.detail

import com.squareup.moshi.Json
import com.worldsnas.cafebazaartest.servermodel.search.CategoryModel
import com.worldsnas.cafebazaartest.servermodel.search.ContactModel
import com.worldsnas.cafebazaartest.servermodel.search.LocationModel
import com.worldsnas.cafebazaartest.servermodel.search.PhotoModel
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class VenueDetailModel(
        @Json(name = "id")
        val id: String,
        @Json(name = "name")
        val name: String,
        @Json(name = "contact")
        val contact: ContactModel?,
        @Json(name = "location")
        val location: LocationModel?,
        @Json(name = "categories")
        val categories: List<CategoryModel>,
        @Json(name = "verified")
        val verified: Boolean,
        @Json(name = "url")
        val url: String?,
        @Json(name = "rate")
        val rate: Int?,
        @Json(name = "photos")
        val photoModel: VenueDetailPhotoModel?,
        @Json(name = "bestPhoto")
        val bestPhoto: PhotoModel?
)