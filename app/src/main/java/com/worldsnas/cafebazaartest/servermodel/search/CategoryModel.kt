package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import com.worldsnas.cafebazaartest.servermodel.search.IconModel
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class CategoryModel(
        @Json(name = "id")
        val id: String,
        @Json(name = "name")
        val name: String,
        @Json(name = "pluralName")
        val pluralName: String,
        @Json(name = "shortName")
        val shortName: String,
        @Json(name = "icon")
        val iconModel: IconModel,
        @Json(name = "primary")
        val primary: Boolean?)