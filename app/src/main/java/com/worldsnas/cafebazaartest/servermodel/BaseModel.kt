package com.worldsnas.cafebazaartest.servermodel

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

//@JsonClass(generateAdapter = BuildConfig.GENERATE_MOSHI_KOTLIN)
@JsonSerializable
data class BaseModel<T>(
        @Json(name = "response")
        val data: T? = null,

        @Json(name = "detail")
        val detail: String? = null
)
