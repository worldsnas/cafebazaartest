package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class LocationModel(
        @Json(name = "lat")
        val lat: Double,
        @Json(name = "lng")
        val lng: Double,
        @Json(name = "distance")
        val distance: Long?,
        @Json(name = "neighborhood")
        val neighborhood: String?,
        @Json(name = "city")
        val city: String?,
        @Json(name = "state")
        val state: String?,
        @Json(name = "country")
        val country: String?,
        @Json(name = "formattedAddress")
        val formattedAddress: List<String>)