package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import com.worldsnas.cafebazaartest.servermodel.search.CategoryModel
import com.worldsnas.cafebazaartest.servermodel.search.LocationModel
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class VenueModel(
        @Json(name = "id")
        val id: String,
        @Json(name = "name")
        val name: String,
        @Json(name = "location")
        val location: LocationModel,
        @Json(name = "categories")
        val categories: List<CategoryModel>,
        @Json(name = "verified")
        val verified: Boolean)