package com.worldsnas.cafebazaartest.servermodel.detail

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class VenueDetailResponseModel (
        @Json(name ="venue")
        val venueDetailModel: VenueDetailModel)