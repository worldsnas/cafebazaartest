package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class IconModel(
        @Json(name = "prefix")
        val prefix: String,
        @Json(name = "suffix")
        val suffix: String)