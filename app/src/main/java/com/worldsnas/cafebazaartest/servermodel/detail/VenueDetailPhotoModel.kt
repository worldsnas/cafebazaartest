package com.worldsnas.cafebazaartest.servermodel.detail

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class VenueDetailPhotoModel(
        @Json(name = "count")
        val count: Long,
        @Json(name = "groups")
        val groups: List<VenueDetailPhotoGroupModel>
)