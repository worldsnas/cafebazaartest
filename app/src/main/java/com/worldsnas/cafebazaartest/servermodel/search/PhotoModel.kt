package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import com.worldsnas.cafebazaartest.servermodel.UserModel
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class PhotoModel(
        @Json(name = "id")
        val id: String,
        @Json(name = "createdAt")
        val createdAt: Int,
        @Json(name = "prefix")
        val prefix: String,
        @Json(name = "suffix")
        val suffix: String,
        @Json(name = "width")
        val width: Int,
        @Json(name = "height")
        val height: Int,
        @Json(name = "visibility")
        val visibility: String,
        @Json(name = "user")
        val user : UserModel?)