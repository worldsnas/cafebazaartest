package com.worldsnas.cafebazaartest.servermodel

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class UserModel(
        @Json(name = "id")
        val id: String,
        @Json(name = "firstName")
        val firstName: String?,
        @Json(name = "lastName")
        val lastName: String?,
        @Json(name = "gender")
        val gender: String)