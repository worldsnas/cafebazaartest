package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class RecommendationResponseModel(
        @Json(name = "group")
        val group: RecomendationGroupModel
)