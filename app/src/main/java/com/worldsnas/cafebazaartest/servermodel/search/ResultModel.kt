package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class ResultModel(
        @Json(name = "venue")
        val venue: VenueModel,
        @Json(name = "photo")
        val photo: PhotoModel?
)