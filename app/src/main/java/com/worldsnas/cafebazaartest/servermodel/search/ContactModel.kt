package com.worldsnas.cafebazaartest.servermodel.search

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class ContactModel(
        @Json(name = "phone")
        val phone: String?,
        @Json(name = "formattedPhone")
        val formattedPhone: String?,
        @Json(name = "twitter")
        val twitter: String?,
        @Json(name = "instagram")
        val instagram: String?,
        @Json(name = "facebook")
        val facebook: String?,
        @Json(name = "facebookUsername")
        val facebookUsername: String?,
        @Json(name = "facebookName")
        val facebookName: String?)