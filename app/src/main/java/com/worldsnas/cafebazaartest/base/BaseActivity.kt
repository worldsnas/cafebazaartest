package com.worldsnas.cafebazaartest.base

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.worldsnas.cafebazaartest.mvibase.MviIntent
import com.worldsnas.cafebazaartest.mvibase.MviView
import com.worldsnas.cafebazaartest.mvibase.MviViewState
import com.worldsnas.cafebazaartest.utils.add
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


abstract class BaseActivity<I : MviIntent, S : MviViewState, V : BaseViewModel<I, S>> : DaggerAppCompatActivity(),
        MviView<I, S> {

    val disposables = CompositeDisposable()

    var viewModelFactory: ViewModelProvider.Factory? = null

    @Inject
    fun injectViewModelFactory(factory: ViewModelProvider.Factory){
        viewModelFactory = factory
    }

    lateinit var viewModel: V

    private fun bind() {
        viewModel.states().subscribe(this::render).add(disposables)
        viewModel.processIntents(intents())
    }

    fun createViewModel(clazz: Class<V>) {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(clazz)
    }

    override fun onStart() {
        super.onStart()
        bind()
    }

    override fun onStop() {
        disposables.clear()
        super.onStop()
    }

    override fun onDestroy() {
        viewModelFactory = null
        super.onDestroy()
    }
}

inline fun <I : MviIntent, S : MviViewState, reified V : BaseViewModel<I, S>> BaseActivity<I, S, V>.createViewModel() {
    createViewModel(V::class.java)
}