package com.worldsnas.cafebazaartest.base

import androidx.annotation.StringRes
import com.worldsnas.cafebazaartest.R

sealed class ErrorHolder {
    class Message(val message: String) : ErrorHolder()
    class StringRes(@androidx.annotation.StringRes val stringID: Int) : ErrorHolder()
    class MessageRes(val message: String, @androidx.annotation.StringRes val stringID: Int) : ErrorHolder()
}

@StringRes
fun ErrorHolder.getStringRes() = when (this) {
    is ErrorHolder.StringRes -> stringID
    else -> R.string.error_unknown
}