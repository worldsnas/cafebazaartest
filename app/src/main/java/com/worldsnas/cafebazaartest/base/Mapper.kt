package com.worldsnas.cafebazaartest.base

interface Mapper<T, R> {

    fun map(item: T): R
}
