package com.worldsnas.cafebazaartest.base

import androidx.lifecycle.ViewModel
import com.worldsnas.cafebazaartest.mvibase.MviIntent
import com.worldsnas.cafebazaartest.mvibase.MviViewModel
import com.worldsnas.cafebazaartest.mvibase.MviViewState
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel<I : MviIntent, S : MviViewState> : ViewModel(),
        MviViewModel<I, S> {

    val disposables by lazy { CompositeDisposable() }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}