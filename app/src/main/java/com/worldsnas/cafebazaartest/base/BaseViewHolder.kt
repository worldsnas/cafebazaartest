package com.worldsnas.cafebazaartest.base

import android.view.View
import io.reactivex.subjects.PublishSubject
import kotlinx.android.extensions.LayoutContainer

abstract class BaseViewHolder<A : BaseViewHolderIntent, K>(override val containerView : View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(containerView), LayoutContainer {

    abstract fun bind(obj : K)

    abstract fun itemOnClick(actionSubject: PublishSubject<A>)

}