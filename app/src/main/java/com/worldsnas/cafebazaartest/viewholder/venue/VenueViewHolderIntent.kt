package com.worldsnas.cafebazaartest.viewholder.venue

import com.worldsnas.cafebazaartest.base.BaseViewHolderIntent

sealed class VenueViewHolderIntent(val adapterPosition: Int) : BaseViewHolderIntent() {
    class OpenVenueDetail(adapterPosition: Int) : VenueViewHolderIntent(adapterPosition)
}