package com.worldsnas.cafebazaartest.viewholder.venue

import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.jakewharton.rxbinding2.view.clicks
import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.base.BaseViewHolder
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.row_venue.*

class VenueViewHolder(view: View) : BaseViewHolder<VenueViewHolderIntent, UiVenue>(view) {

    override fun bind(obj: UiVenue) {

        val request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(obj.photo))
                .setResizeOptions(ResizeOptions(250, 250))
                .build()
        val controller = Fresco.newDraweeControllerBuilder()
                .setOldController(photo.controller)
                .setImageRequest(request)
                .build()
        photo.controller = controller
//        photo.setImageURI(obj.photo)
        name.text = obj.name
        cat.text = obj.cat
        location.text = obj.location
        val icon = if (obj.verified) {
            R.drawable.ic_check_black_24dp
        } else {
            R.drawable.ic_close_black_24dp
        }
        verified.setImageResource(icon)
    }

    override fun itemOnClick(actionSubject: PublishSubject<VenueViewHolderIntent>) {
        itemView.clicks()
                .map { adapterPosition }
                .filter { it != RecyclerView.NO_POSITION }
                .map { VenueViewHolderIntent.OpenVenueDetail(it) }
                .subscribe(actionSubject)
    }
}