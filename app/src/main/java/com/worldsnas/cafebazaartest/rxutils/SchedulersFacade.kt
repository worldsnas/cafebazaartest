package com.worldsnas.cafebazaartest.rxutils

import io.reactivex.Scheduler

/**
 * Provides various threading schedulers.
 */

interface SchedulersFacade {
    /**
     * IO thread pool scheduler
     */
    fun io(): Scheduler

    /**
     * Computation thread pool scheduler
     */
    fun computation(): Scheduler

    fun single(): Scheduler

    /**
     * Main Thread scheduler
     */
    fun ui(): Scheduler
}