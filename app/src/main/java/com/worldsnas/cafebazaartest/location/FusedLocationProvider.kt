package com.worldsnas.cafebazaartest.location

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
import java.lang.ref.SoftReference

class FusedLocationProvider(
        private val context: Context,
        private val fusedLocationProvider: FusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(context)
) : LocationProvider {

    private var isRegistered = false
    private var locationCallback : LocationCallback? = null

    private val permissions: Array<out String> = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private val subscribers = mutableListOf<(latitude: Double, longitude: Double) -> Unit>()

    override fun requestUpdates(callback: (latitude: Double, longitude: Double) -> Unit) {
        subscribers += callback
        if (!isRegistered && (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || permissions.all { ActivityCompat.checkSelfPermission(context, it) == PERMISSION_GRANTED })) {
            isRegistered = true
            LocationRequest().apply {
                interval = 1000 * 60
                fastestInterval = 1000 * 30
                priority = PRIORITY_BALANCED_POWER_ACCURACY
            }.apply {
                locationCallback = getLocationCallback()
                fusedLocationProvider.requestLocationUpdates(this, locationCallback, null)
            }
        }
    }

    override fun cancelUpdates(callback: (latitude: Double, longitude: Double) -> Unit) {
        subscribers -= callback
        subscribers.clear()
        if (subscribers.isEmpty()) {
            fusedLocationProvider.removeLocationUpdates(locationCallback)
            isRegistered = false
            locationCallback = null
        }
    }

    private fun getLocationCallback() = LocationCallbackReference(object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult?.locations?.firstOrNull()?.also { location ->
                val latitude = location.latitude
                val longitude = location.longitude
                subscribers.forEach { callback ->
                    callback(latitude, longitude)
                }
            }
        }
    })

}

class LocationCallbackReference(locationCallback: LocationCallback) : LocationCallback(){

    private val callback : SoftReference<LocationCallback> = SoftReference(locationCallback)

    override fun onLocationResult(p0: LocationResult?) {
        super.onLocationResult(p0)
        callback.get()?.onLocationResult(p0)
    }

    override fun onLocationAvailability(p0: LocationAvailability?) {
        super.onLocationAvailability(p0)
        callback.get()?.onLocationAvailability(p0)
    }

}