package com.worldsnas.cafebazaartest.location

import com.worldsnas.cafebazaartest.activity.main.uimodels.LatLang
import io.reactivex.Observable

class RxLocationProvider(private val locationProvider: LocationProvider) {

    private val locationCallback: (latitude: Double, longitude: Double) -> Unit = { latitude, longitude ->
        LatLang.create(latitude, longitude)
    }

    fun updates(): Observable<LatLang> = Observable.create {
        val locationCallback: (latitude: Double, longitude: Double) -> Unit = { latitude, longitude ->
            if (!it.isDisposed) {
                it.onNext(LatLang.create(latitude, longitude))
            }
        }
        locationProvider.requestUpdates(locationCallback)

        it.setCancellable {
            locationProvider.cancelUpdates(locationCallback)
        }
    }
}

fun LocationProvider.updates() =
        RxLocationProvider(this).updates()
