package com.worldsnas.cafebazaartest.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "detail_venue", indices = [Index(value= ["venue_id"], unique = true)])
data class DetailVenueEntity(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name= "id")
        var id: Int = 0,
        @ColumnInfo(name= "venue_id")
        var venue_id: String = "",
        @ColumnInfo(name = "name")
        val name: String = "",
        @ColumnInfo(name = "category")
        val cat: String = "",
        @ColumnInfo(name = "phone")
        val phone: String = "",
        @ColumnInfo(name = "category_icon")
        val catIcon: String = "",
        @ColumnInfo(name = "address")
        val address: String = "",
        @ColumnInfo(name = "city")
        val city: String = "",
        @ColumnInfo(name = "rate")
        val rate: String = "",
        @ColumnInfo(name = "best_photo")
        val bestPhoto: String = "",
        @ColumnInfo(name = "cache_time")
        var cacheTime: Long = 0)