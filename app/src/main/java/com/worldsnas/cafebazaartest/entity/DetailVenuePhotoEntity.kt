package com.worldsnas.cafebazaartest.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "venue_photo")
data class DetailVenuePhotoEntity(
        @PrimaryKey
        val id: Long = 0,
        @ColumnInfo(name = "venue_id")
        val venueId: Long = 0,
        @ColumnInfo(name = "url")
        val url: String = "",
        @ColumnInfo(name = "uploader")
        val uploader: String = "")