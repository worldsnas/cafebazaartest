package com.worldsnas.cafebazaartest.entity

import androidx.room.Embedded
import androidx.room.Relation

data class DetailVenueWithPhoto(
        @Embedded val venue: DetailVenueEntity,
        @Relation(parentColumn = "id",
                entityColumn = "venue_id") val photos: List<DetailVenuePhotoEntity>
)