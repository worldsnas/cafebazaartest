package com.worldsnas.cafebazaartest.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_venue", indices = [androidx.room.Index(value = ["venue_id"], unique = true)])
data class SearchVenueEntity(@PrimaryKey(autoGenerate = true)
                             var id: Long = 0,
                             @ColumnInfo(name = "venue_id")
                             var venueId: String = "",
                             @ColumnInfo(name = "name")
                             var name: String = "",
                             @ColumnInfo(name = "category")
                             var category: String = "",
                             @ColumnInfo(name = "photo")
                             var photo: String = "",
                             @ColumnInfo(name = "lat_lang")
                             var latLng: String = "",
                             @ColumnInfo(name = "verified")
                             var verified: Boolean = false,
                             @ColumnInfo(name = "cache_time")
                             var cacheTime: Long = 0,
                             @ColumnInfo(name = "user_lat_lang")
                             var userLatLang: String = "",
                             @ColumnInfo(name = "page")
                             var page: Int = 0)