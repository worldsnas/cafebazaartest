package com.worldsnas.cafebazaartest.activity.main.processormodels

import com.worldsnas.cafebazaartest.activity.main.uimodels.LatLang
import com.worldsnas.cafebazaartest.mvibase.MviAction
import com.worldsnas.cafebazaartest.utils.Load

sealed class MainActivityAction : MviAction{
    class Permission(val hasPermission : Boolean, val userProvided : Boolean) : MainActivityAction()
    class LoadLocations(val location : LatLang, val load : Load) : MainActivityAction()
    class OpenDetailAction(val position : Int) : MainActivityAction()
}