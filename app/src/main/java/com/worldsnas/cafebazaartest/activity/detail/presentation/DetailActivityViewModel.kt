package com.worldsnas.cafebazaartest.activity.detail.presentation

import com.worldsnas.cafebazaartest.activity.detail.processormodels.DetailActivityAction
import com.worldsnas.cafebazaartest.activity.detail.processormodels.DetailActivityResult
import com.worldsnas.cafebazaartest.activity.detail.uimodels.DetailActivityIntent
import com.worldsnas.cafebazaartest.activity.detail.uimodels.DetailActivityState
import com.worldsnas.cafebazaartest.base.BaseViewModel
import com.worldsnas.cafebazaartest.mvibase.MviProcessor
import com.worldsnas.cafebazaartest.utils.notOfType
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class DetailActivityViewModel @Inject constructor(
        private val processor: MviProcessor<DetailActivityAction, DetailActivityResult>
) : BaseViewModel<DetailActivityIntent, DetailActivityState>() {

    private val intentFilter = ObservableTransformer<DetailActivityIntent, DetailActivityIntent> { intents ->
        intents.publish { share ->
            Observable.merge(share.ofType(DetailActivityIntent.InitialIntent::class.java).take(1),
                    share.notOfType(DetailActivityIntent.InitialIntent::class.java)
            )
        }
    }

    private val intentMapper = Function<DetailActivityIntent, DetailActivityAction> { intent ->
        when (intent) {
            is DetailActivityIntent.InitialIntent -> DetailActivityAction.InitialAction(intent.venue)
            is DetailActivityIntent.OpenMapClickedIntent-> DetailActivityAction.OpenOnMapAction(intent.venue)
        }
    }

    private val reducer = BiFunction<DetailActivityState, DetailActivityResult, DetailActivityState> { preState, result ->
        when (result) {
            is DetailActivityResult.InitialResult -> {
                preState.copy(loading = false, showError = false, showMap = false, searchedVenue = result.searchVenue)
            }
            is DetailActivityResult.InFlight -> {
                preState.copy(loading = true, showError = false, showMap = false)
            }
            is DetailActivityResult.LastStableResult-> {
                preState.copy(loading = false, showError = false, showMap = false)
            }
            is DetailActivityResult.OpenMapResult-> {
                preState.copy(loading = false, showError = false, showMap = true, mapUrl = result.url)
            }
            is DetailActivityResult.DetailReceivedResult-> {
                preState.copy(loading = false, showError = false, showMap = false, detailVenue = result.venueDetail, showSlides = true, slides = result.photos)
            }
            is DetailActivityResult.ErrorResult-> {
                preState.copy(loading = false, showMap = false, showError = true, errorStringId = result.errorStringId)
            }
        }
    }

    private val intentsSubject: PublishSubject<DetailActivityIntent> = PublishSubject.create()
    private val statesObservable: Observable<DetailActivityState> = compose()

    private fun compose(): Observable<DetailActivityState> {
        return intentsSubject
                .compose(intentFilter)
                .map(intentMapper)
                .compose(processor.actionProcessor)
                .scan(DetailActivityState.init(), reducer)
//                .skip(1)
                .doOnNext { Timber.d("new state created= $it") }
                .distinctUntilChanged()
                .replay(1)
                .autoConnect(0)
    }

    override fun states(): Observable<DetailActivityState> = statesObservable

    override fun processIntents(intents: Observable<DetailActivityIntent>) {
        intents.subscribe(intentsSubject)
    }
}