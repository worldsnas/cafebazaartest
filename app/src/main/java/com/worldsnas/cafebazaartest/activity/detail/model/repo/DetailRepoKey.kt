package com.worldsnas.cafebazaartest.activity.detail.model.repo

data class DetailRepoKey(val venueId : String) {

    companion object {
        @JvmStatic
        fun create(venueId: String) = DetailRepoKey(venueId)
    }

}