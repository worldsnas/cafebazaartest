package com.worldsnas.cafebazaartest.activity.detail.model.repo.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.worldsnas.cafebazaartest.entity.DetailVenueEntity
import com.worldsnas.cafebazaartest.entity.DetailVenuePhotoEntity
import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single


@Dao
abstract class DetailVenueDao {

    @Query("SELECT * from detail_venue where venue_id = :venueId AND cache_time >= :yesterday")
    abstract fun getVenueWithPhotos(venueId :String, yesterday : Long) : Observable<DetailVenueWithPhoto>

    @Query("SELECT * from detail_venue where venue_id = :venueId AND cache_time >= :yesterday")
    abstract fun getSingleVenueWithPhotos(venueId :String, yesterday : Long) : Single<DetailVenueWithPhoto>

    fun insert(venues: DetailVenueWithPhoto) : Completable {
        val id = insertVenue(venues.venue)
        val photos = venues.photos.map {
            it.copy(venueId= id)
        }

        return insertAllPhoto(photos)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAllPhoto(pets: List<DetailVenuePhotoEntity>)  : Completable //this could go in a PetDao instead...

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertVenue(user: DetailVenueEntity): Long

    @Query("DELETE FROM detail_venue WHERE venue_id = :venueId")
    abstract fun deleteByVenueId(venueId: String) : Int

    @Query("DELETE FROM detail_venue WHERE cache_time >= :yesterday")
    abstract fun deleteAllStale(yesterday : Long) : Int

}
