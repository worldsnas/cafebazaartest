package com.worldsnas.cafebazaartest.activity.main.model.repo

import com.worldsnas.cafebazaartest.activity.main.uimodels.LatLang

data class MainRepoKey(val location: LatLang,
                       val offset: Int) {

    companion object {
        @JvmStatic
        fun create(location: LatLang, offset: Int) = MainRepoKey(location, offset)
    }

}