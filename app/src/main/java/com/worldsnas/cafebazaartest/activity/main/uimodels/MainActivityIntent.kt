package com.worldsnas.cafebazaartest.activity.main.uimodels

import com.worldsnas.cafebazaartest.mvibase.MviIntent
import com.worldsnas.cafebazaartest.utils.Load

sealed class MainActivityIntent : MviIntent {

    class InitialIntent(val hasPermission : Boolean) : MainActivityIntent()
    class UserPermission(val granted : Boolean) : MainActivityIntent()
    class VenueClickIntent(val position: Int) : MainActivityIntent()
    class LoadMore(val location : LatLang, val load : Load) : MainActivityIntent()
    class LocationUpdate(val location : LatLang) : MainActivityIntent()
}