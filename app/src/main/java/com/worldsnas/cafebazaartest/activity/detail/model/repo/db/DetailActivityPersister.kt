package com.worldsnas.cafebazaartest.activity.detail.model.repo.db

import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailRepoKey
import com.worldsnas.cafebazaartest.app.db.AppDataBase
import com.worldsnas.cafebazaartest.base.ErrorHolder
import com.worldsnas.cafebazaartest.utils.getYesterday
import com.worldsnas.panther.Persister
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class DetailActivityPersister @Inject constructor(private val db: AppDataBase) : Persister<DetailRepoKey, DetailActivityDBModel> {

    override fun read(key: DetailRepoKey): Single<DetailActivityDBModel> =
            db
                    .detailVenue()
                    .getSingleVenueWithPhotos(key.venueId, yesterday = getYesterday())
                    .map {
                        DetailActivityDBModel.Success(it) as DetailActivityDBModel
                    }
                    .onErrorReturnItem(DetailActivityDBModel.Error(ErrorHolder.StringRes(R.string.error_not_found)))
                    .doOnEvent { data, t ->
                        Timber.d("reading from database= $data")
                        Timber.e(t, "reading form database")
                    }

    override fun write(key: DetailRepoKey, dbModel: DetailActivityDBModel): Single<Boolean> {
        val venue = when (dbModel) {
            is DetailActivityDBModel.Success -> dbModel.venue
            else -> null
        } ?: return Single.just(false)

        return db
                .detailVenue()
                .insert(venue)
                .andThen(Single.just(true))
                .onErrorReturnItem(false)
    }

    override fun observe(key: DetailRepoKey): Observable<DetailActivityDBModel> =
            db
                    .detailVenue()
                    .getVenueWithPhotos(key.venueId, yesterday = getYesterday())
                    .map {
                        DetailActivityDBModel.Success(it) as DetailActivityDBModel
                    }
                    .onErrorReturnItem(DetailActivityDBModel.Error(ErrorHolder.StringRes(R.string.error_not_found)))

    override fun clear(key: DetailRepoKey): Single<Boolean> =
            Single
                    .create<Int> {
                        val deleteds = db.detailVenue()
                                .deleteAllStale(getYesterday())

                        it.onSuccess(deleteds)
                    }
                    .map {
                        it > 0
                    }
                    .onErrorReturnItem(false)


    override fun clearWithModel(dbModel: DetailActivityDBModel): Single<Boolean> {
        val venueId = when (dbModel) {
            is DetailActivityDBModel.Success -> dbModel.venue.venue.venue_id
            else -> null
        } ?: return Single.just(false)

        return Single
                .create<Int> {
                    val deleteds = db
                            .detailVenue()
                            .deleteByVenueId(venueId = venueId)

                    it.onSuccess(deleteds)
                }
                .map {
                    it > 0
                }
                .onErrorReturnItem(false)
    }

}