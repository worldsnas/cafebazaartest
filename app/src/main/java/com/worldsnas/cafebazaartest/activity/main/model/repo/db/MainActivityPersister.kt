package com.worldsnas.cafebazaartest.activity.main.model.repo.db

import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.main.model.repo.MainRepoKey
import com.worldsnas.cafebazaartest.activity.main.uimodels.LatLang
import com.worldsnas.cafebazaartest.app.db.AppDataBase
import com.worldsnas.cafebazaartest.base.ErrorHolder
import com.worldsnas.cafebazaartest.utils.SEARCH_LIMIT
import com.worldsnas.cafebazaartest.utils.getYesterday
import com.worldsnas.cafebazaartest.utils.locationDistance
import com.worldsnas.panther.Persister
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class MainActivityPersister @Inject constructor(private val db: AppDataBase) : Persister<MainRepoKey, MainActivityDBModel> {

    override fun read(key: MainRepoKey): Single<MainActivityDBModel> =
            db.searchVenue().getNotStaleSingle(getYesterday(), key.offset)
                    .map { venues ->
                        venues.filter { venue ->
                            isValidForLocation(key.location, LatLang.parseString(venue.userLatLang))
                        }
                    }
                    .map {
                        MainActivityDBModel.Success(it) as MainActivityDBModel
                    }
                    .doOnError {
                        Timber.e(it, "reading venues from db failed")
                    }
                    .onErrorReturnItem(
                            MainActivityDBModel.Error(ErrorHolder.StringRes(R.string.error_db_failed))
                    )


    override fun write(key: MainRepoKey, dbModel: MainActivityDBModel): Single<Boolean> {
        val venues = when (dbModel) {
            is MainActivityDBModel.Success -> {
                dbModel.venues
            }
            else -> {
                null
            }
        } ?: return Single.just(false)

        venues.forEach {
            it.userLatLang = key.location.toString()
            it.cacheTime = System.currentTimeMillis()
            it.page = key.offset
        }

        return db.searchVenue()
                .insertAll(venues)
                .andThen(Single.just(true))
                .onErrorReturnItem(false)
    }

    override fun observe(key: MainRepoKey): Observable<MainActivityDBModel> =
            db.searchVenue().getNotStale(getYesterday(), key.offset)
                    .map {
                        MainActivityDBModel.Success(it) as MainActivityDBModel
                    }
                    .onErrorReturn {
                        MainActivityDBModel.Error(ErrorHolder.StringRes(R.string.error_db_failed))
                    }

    override fun clear(key: MainRepoKey): Single<Boolean> =
            db.searchVenue().getStale(getYesterday())
                    .firstOrError()
                    .flatMap {
                        db.searchVenue().deleteAll(it)
                        Single.just(true)
                    }
                    .onErrorReturnItem(false)


    override fun clearWithModel(dbModel: MainActivityDBModel): Single<Boolean> {
        val venues = when (dbModel) {
            is MainActivityDBModel.Success -> {
                dbModel.venues
            }
            else -> {
                null
            }
        } ?: return Single.just(false)

        db.searchVenue().deleteAll(venues)

        return Single.just(true)
    }

    private fun isValidForLocation(current: LatLang, cache: LatLang): Boolean =
            locationDistance(current.lat, cache.lat, current.lng, cache.lng) < 100.0

}