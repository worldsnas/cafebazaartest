package com.worldsnas.cafebazaartest.activity.main.model.repo

import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.base.ErrorHolder

sealed class MainActivityRepoModel {

    class Success(val venues : List<UiVenue>) : MainActivityRepoModel()
    class Error(val error : ErrorHolder) : MainActivityRepoModel()
}