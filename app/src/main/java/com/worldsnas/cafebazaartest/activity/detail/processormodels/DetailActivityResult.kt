package com.worldsnas.cafebazaartest.activity.detail.processormodels

import com.worldsnas.cafebazaartest.activity.detail.uimodels.UiVenueDetail
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.mvibase.MviResult

sealed class DetailActivityResult : MviResult {

    class OpenMapResult(val url : String) : DetailActivityResult()
    class ErrorResult(val errorStringId: Int) : DetailActivityResult()
    class DetailReceivedResult(val venueDetail: UiVenueDetail, val photos : Map<String, String>) : DetailActivityResult()
    class InitialResult(val searchVenue: UiVenue) : DetailActivityResult()
    object LastStableResult : DetailActivityResult()
    object InFlight : DetailActivityResult()
}