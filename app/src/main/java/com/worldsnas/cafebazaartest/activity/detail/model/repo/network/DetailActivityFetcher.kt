package com.worldsnas.cafebazaartest.activity.detail.model.repo.network

import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailRepoKey
import com.worldsnas.cafebazaartest.app.network.retrofitutils.RetrofitRetryHandler
import com.worldsnas.cafebazaartest.app.network.retrofitutils.createErrorResponse
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailResponseModel
import com.worldsnas.cafebazaartest.utils.API_VERSION
import com.worldsnas.cafebazaartest.utils.CLIENT_ID
import com.worldsnas.cafebazaartest.utils.CLIENT_SECRET
import com.worldsnas.panther.Fetcher
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

class DetailActivityFetcher @Inject constructor(private val api: DetailApi) : Fetcher<DetailRepoKey, Response<BaseModel<VenueDetailResponseModel>>> {

    override fun fetch(key: DetailRepoKey): Single<Response<BaseModel<VenueDetailResponseModel>>> =
            api.getVenueDetail(key.venueId,
                    CLIENT_ID,
                    CLIENT_SECRET,
                    API_VERSION)
                    .retry(RetrofitRetryHandler.createDefault())
                    .onErrorReturn {
                        createErrorResponse(it)
                    }

}