package com.worldsnas.cafebazaartest.activity.detail.processormodels

import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.mvibase.MviAction

sealed class DetailActivityAction : MviAction {

    class OpenOnMapAction(val searchVenue : UiVenue) : DetailActivityAction()
    class InitialAction(val searchVenue : UiVenue) : DetailActivityAction()
}