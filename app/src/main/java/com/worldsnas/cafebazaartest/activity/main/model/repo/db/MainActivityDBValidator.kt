package com.worldsnas.cafebazaartest.activity.main.model.repo.db

import com.worldsnas.cafebazaartest.activity.main.model.repo.MainRepoKey
import com.worldsnas.panther.DBValidator
import javax.inject.Inject

class MainActivityDBValidator @Inject constructor() : DBValidator<MainRepoKey, MainActivityDBModel> {
    override fun isEmpty(key: MainRepoKey, dbModel: MainActivityDBModel): Boolean =
            when (dbModel) {
                is MainActivityDBModel.Success -> {
                    dbModel.venues.isEmpty()
                }
                else -> {
                    true
                }
            }
}