package com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper

import androidx.collection.ArrayMap
import com.worldsnas.cafebazaartest.base.Mapper
import com.worldsnas.cafebazaartest.entity.DetailVenuePhotoEntity
import javax.inject.Inject

class PhotoEntityUiMapper @Inject constructor():Mapper<List<DetailVenuePhotoEntity>, Map<String, String>>{
    override fun map(item: List<DetailVenuePhotoEntity>): Map<String, String> {
        val map = ArrayMap<String, String>()
        item.forEach {
            map[it.url] = it.uploader
        }
        return map
    }
}