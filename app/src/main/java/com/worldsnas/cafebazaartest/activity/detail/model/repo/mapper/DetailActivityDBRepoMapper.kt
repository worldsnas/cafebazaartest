package com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper

import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailActivityRepoModel
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailActivityDBModel
import com.worldsnas.cafebazaartest.activity.detail.uimodels.UiVenueDetail
import com.worldsnas.cafebazaartest.base.BiMapper
import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto
import com.worldsnas.panther.DBMapper
import javax.inject.Inject

class DetailActivityDBRepoMapper @Inject constructor(
        private val mapper: BiMapper<DetailVenueWithPhoto, UiVenueDetail>,
        private val photoMapper: PhotoEntityUiMapper
) : DBMapper<DetailActivityDBModel, DetailActivityRepoModel> {

    override fun mapDb(dbModel: DetailActivityDBModel): DetailActivityRepoModel =
            when (dbModel) {
                is DetailActivityDBModel.Success -> {
                    DetailActivityRepoModel.Success(mapper.mapTo(dbModel.venue), photoMapper.map(dbModel.venue.photos))
                }
                is DetailActivityDBModel.Error -> {
                    DetailActivityRepoModel.Error(dbModel.error)
                }
            }

    override fun mapRepo(repoModel: DetailActivityRepoModel): DetailActivityDBModel =
            when (repoModel) {
                is DetailActivityRepoModel.Success -> {
                    DetailActivityDBModel.Success(TODO("NO Time"))
                }
                is DetailActivityRepoModel.Error -> {
                    DetailActivityDBModel.Error(repoModel.error)
                }
            }


}