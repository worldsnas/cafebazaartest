package com.worldsnas.cafebazaartest.activity.main.uimodels

data class LatLang(val lat: Double,
                   val lng: Double) {
    companion object {
        @JvmStatic
        fun create(lat: Double, lng: Double) = LatLang(lat, lng)

        @JvmStatic
        fun parseString(location: String): LatLang {
            val locs = location.split(",")
            if (locs.size != 2) {
                throw IllegalArgumentException("the location=$location is not a valid format")
            }

            return create(locs[0].toDouble(), locs[1].toDouble())
        }
    }

    override fun toString(): String {
        return "$lat,$lng"
    }
}