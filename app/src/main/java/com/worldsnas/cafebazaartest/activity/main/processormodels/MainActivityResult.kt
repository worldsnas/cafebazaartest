package com.worldsnas.cafebazaartest.activity.main.processormodels

import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.mvibase.MviResult

sealed class MainActivityResult : MviResult{
    object StartLocationUpdates : MainActivityResult()
    object GetPermission: MainActivityResult()
    object CloseActivity : MainActivityResult()
    class Venues(val venues : List<UiVenue>) : MainActivityResult()
    class Error(val stringId : Int) : MainActivityResult()
    object LastStableResult : MainActivityResult()
    object InFlight : MainActivityResult()
    class OpenDetail(val venue : UiVenue) : MainActivityResult()
}