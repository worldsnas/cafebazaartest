package com.worldsnas.cafebazaartest.activity.detail.model

import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailActivityRepoModel
import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailRepoKey
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailActivityDBModel
import com.worldsnas.cafebazaartest.activity.detail.processormodels.DetailActivityAction
import com.worldsnas.cafebazaartest.activity.detail.processormodels.DetailActivityResult
import com.worldsnas.cafebazaartest.base.getStringRes
import com.worldsnas.cafebazaartest.mvibase.MviProcessor
import com.worldsnas.cafebazaartest.rxutils.SchedulersFacade
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailResponseModel
import com.worldsnas.panther.Panther
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DetailActivityProcessorHolder @Inject constructor(
        private val panther: Panther<DetailActivityRepoModel, Response<BaseModel<VenueDetailResponseModel>>, DetailActivityDBModel, DetailRepoKey>,
        private val schedulersFacade: SchedulersFacade
) : MviProcessor<DetailActivityAction, DetailActivityResult> {

    private val loadDetailProcessor = ObservableTransformer<DetailActivityAction.InitialAction, DetailActivityResult> { action ->
        action.flatMap { initial ->
            val venue = initial.searchVenue
            panther.get(DetailRepoKey.create(venue.venueId)).toObservable()
                    .map {
                        when (it) {
                            is DetailActivityRepoModel.Success -> DetailActivityResult.DetailReceivedResult(it.venueDetail, it.photos)
                            is DetailActivityRepoModel.Error -> DetailActivityResult.ErrorResult(it.error.getStringRes())
                        }
                    }
                    .switchMap {
                        when (it) {
                            is DetailActivityResult.ErrorResult -> {
                                Observable.just(DetailActivityResult.LastStableResult as DetailActivityResult)
                                        .delay(3, TimeUnit.SECONDS)
                                        .startWith(it)
                            }
                            else -> Observable.just(it)
                        }
                    }
                    .subscribeOn(schedulersFacade.io())
                    .observeOn(schedulersFacade.ui())
        }
    }

    private val initialProcessor = ObservableTransformer<DetailActivityAction.InitialAction, DetailActivityResult> { action ->
        action.publish { share ->
            Observable.merge(share.map { DetailActivityResult.InitialResult(it.searchVenue) },
                    share.map { DetailActivityResult.InFlight },
                    share.compose(loadDetailProcessor))
        }
    }

    private val openMapProcessor = ObservableTransformer<DetailActivityAction.OpenOnMapAction, DetailActivityResult> { action ->
        action.switchMap {
            Observable.just(DetailActivityResult.LastStableResult as DetailActivityResult)
                    .delay(3, TimeUnit.SECONDS)
                    .startWith(DetailActivityResult.OpenMapResult(getMapUriString(it.searchVenue.location)))
        }
    }


    override val actionProcessor: ObservableTransformer<DetailActivityAction, DetailActivityResult>
        get() = ObservableTransformer { actions ->
            Observable.merge(
                    actions.ofType(DetailActivityAction.InitialAction::class.java).compose(initialProcessor),
                    actions.ofType(DetailActivityAction.OpenOnMapAction::class.java).compose(openMapProcessor)
            )
        }


    private fun getMapUriString(location: String) =
            String.format(Locale.ENGLISH, "geo:%s", location)

}