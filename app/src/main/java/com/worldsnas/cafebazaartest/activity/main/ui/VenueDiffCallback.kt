package com.worldsnas.cafebazaartest.activity.main.ui

import androidx.recyclerview.widget.DiffUtil
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import javax.inject.Inject

class VenueDiffCallback @Inject constructor(): DiffUtil.ItemCallback<UiVenue>(){

    override fun areItemsTheSame(oldItem: UiVenue, newItem: UiVenue): Boolean =
        oldItem.venueId == newItem.venueId


    override fun areContentsTheSame(oldItem: UiVenue, newItem: UiVenue): Boolean = oldItem.toString() == newItem.toString()
}