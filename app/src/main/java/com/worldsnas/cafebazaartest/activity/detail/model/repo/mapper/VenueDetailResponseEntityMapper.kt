package com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper

import com.worldsnas.cafebazaartest.base.Mapper
import com.worldsnas.cafebazaartest.entity.DetailVenueEntity
import com.worldsnas.cafebazaartest.entity.DetailVenuePhotoEntity
import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto
import com.worldsnas.cafebazaartest.servermodel.UserModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailPhotoModel
import com.worldsnas.cafebazaartest.servermodel.search.CategoryModel
import com.worldsnas.cafebazaartest.servermodel.search.LocationModel
import com.worldsnas.cafebazaartest.servermodel.search.PhotoModel
import javax.inject.Inject

class VenueDetailResponseEntityMapper @Inject constructor() : Mapper<VenueDetailModel, DetailVenueWithPhoto> {
    override fun map(item: VenueDetailModel): DetailVenueWithPhoto {
        val venue = DetailVenueEntity(0,
                item.id,
                item.name,
                getPrimaryCategory(item.categories),
                item.contact?.phone?: "",
                getPrimaryCategoryIcon(item.categories),
                getAddress(item.location),
                item.location?.city ?: "",
                item.rate?.toString()?: "",
                getBestPhoto(item.bestPhoto),
                System.currentTimeMillis())

        val photos = getPhotos(item.photoModel)

        return DetailVenueWithPhoto(venue, photos)
    }

    private fun getPrimaryCategory(categories: List<CategoryModel>?): String {
        if (categories == null || categories.isEmpty())
            return ""
        return categories.find { it.primary?: false }?.name ?: categories[0].name ?: ""
    }

    private fun getPrimaryCategoryIcon(categories: List<CategoryModel>?): String {
        if (categories == null || categories.isEmpty())
            return ""
        val iconModel = categories.find { it.primary?: false }?.iconModel ?: categories[0].iconModel

        return iconModel.prefix + "bg_32" + iconModel.suffix
    }

    private fun getAddress(location: LocationModel?): String {
        if (location == null)
            return ""

        return location.formattedAddress.toString().let { it.substring(1, it.length -1) }
    }

    private fun getBestPhoto(photo: PhotoModel?): String {
        if (photo == null)
            return ""

        return photo.prefix + "original" + photo.suffix
    }

    private fun getPhotoUser(user : UserModel?): String{
        if (user == null) {
            return ""
        }

        return user.firstName + "original" + user.lastName
    }

    private fun getPhotos(group: VenueDetailPhotoModel?): List<DetailVenuePhotoEntity> {
        if (group == null || group.count == 0L) {
            return emptyList()
        }

        val photoGroups = group.groups

        val photoEntities = ArrayList<DetailVenuePhotoEntity>()

        photoGroups.forEach { photoGroup ->
            if (photoGroup.count != 0L) {
                photoGroup.photos.forEach {photo->
                    photoEntities.add(DetailVenuePhotoEntity(
                            0,
                            0,
                            getBestPhoto(photo),
                            getPhotoUser(photo.user)
                    ))
                }
            }
        }

        return photoEntities
    }
}