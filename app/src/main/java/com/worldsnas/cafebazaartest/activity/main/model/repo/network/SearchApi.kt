package com.worldsnas.cafebazaartest.activity.main.model.repo.network

import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.search.RecommendationResponseModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("/v2/search/recommendations")
    fun explore(@Query("client_id") clientId : String,
                @Query("client_secret") clientSecret : String,
                @Query("v") version : String,
                @Query("photos") intent : String,
                @Query("limit") limit: Int,
                @Query("ll") latLng : String,
                @Query("offset") offset : Int
    ): Single<Response<BaseModel<RecommendationResponseModel>>>
}