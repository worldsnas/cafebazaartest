package com.worldsnas.cafebazaartest.activity.main.model.repo.mapper

import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.base.BiMapper
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity
import javax.inject.Inject

class SearchVenueEntityUIMapper @Inject constructor(): BiMapper<SearchVenueEntity, UiVenue>{

    override fun mapTo(item: SearchVenueEntity): UiVenue =
            UiVenue(item.id,
                    item.venueId,
                    item.name,
                    item.category,
                    item.latLng,
                    item.photo,
                    item.verified,
                    item.page)

    override fun mapBack(item: UiVenue): SearchVenueEntity =
            SearchVenueEntity(item.dbId,
                    item.venueId,
                    item.name,
                    item.cat,
                    item.location,
                    item.photo,
                    item.verified)
}