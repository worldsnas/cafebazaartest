package com.worldsnas.cafebazaartest.activity.detail.model.repo.network

import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailResponseModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DetailApi {

    @GET("/v2/venues/{venue_id}")
    fun getVenueDetail(@Path("venue_id") venueId: String,
                       @Query("client_id") clientId: String,
                       @Query("client_secret") clientSecret: String,
                       @Query("v") version: String): Single<Response<BaseModel<VenueDetailResponseModel>>>
}