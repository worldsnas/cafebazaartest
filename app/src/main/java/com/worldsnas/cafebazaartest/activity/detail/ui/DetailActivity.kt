package com.worldsnas.cafebazaartest.activity.detail.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.google.android.material.appbar.AppBarLayout
import com.jakewharton.rxbinding2.view.clicks
import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.detail.presentation.DetailActivityViewModel
import com.worldsnas.cafebazaartest.activity.detail.uimodels.DetailActivityIntent
import com.worldsnas.cafebazaartest.activity.detail.uimodels.DetailActivityState
import com.worldsnas.cafebazaartest.activity.detail.uimodels.UiVenueDetail
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.base.BaseActivity
import com.worldsnas.cafebazaartest.base.createViewModel
import com.worldsnas.cafebazaartest.utils.SEARCH_VENUE_PARCELABLE
import com.worldsnas.cafebazaartest.utils.setVisibility
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*
import timber.log.Timber


class DetailActivity : BaseActivity<DetailActivityIntent, DetailActivityState, DetailActivityViewModel>() {

    var menu: Menu? = null
    lateinit var searchedVenue: UiVenue
    var mapUrl: String? = null

    private val openMapsPS = PublishSubject.create<DetailActivityIntent.OpenMapClickedIntent>()

    companion object {
        @JvmStatic
        fun createIntent(context: Context, uiVenue: UiVenue) =
                Intent(context, DetailActivity::class.java)
                .also {
                    it.putExtra(SEARCH_VENUE_PARCELABLE, uiVenue)
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createViewModel()
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(Color.WHITE)
        toolbar_layout.setCollapsedTitleTextColor(Color.WHITE)
        toolbar_layout.setExpandedTitleColor(Color.WHITE)
        app_bar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true
                    showOption(R.id.action_info)
                } else if (isShow) {
                    isShow = false
                    hideOption(R.id.action_info)
                }
//                statusBG.setVisibility(isShow)
            }
        })
        searchedVenue = getSearchVenue()


        initOpenMapFabClicks()
    }

    override fun onStart() {
        super.onStart()
        slider.startAutoCycle()
    }

    override fun onStop() {
        slider.stopAutoCycle()
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.menu_scrolling, menu)
        hideOption(R.id.action_info)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_info) {
            openMapsPS.onNext(DetailActivityIntent.OpenMapClickedIntent(searchedVenue))
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun hideOption(id: Int) {
        val item = menu?.findItem(id)
        item?.isVisible = false
    }

    private fun showOption(id: Int) {
        val item = menu?.findItem(id)
        item?.isVisible = true
    }

    private fun getSearchVenue() = intent.getParcelableExtra<UiVenue>(SEARCH_VENUE_PARCELABLE)
            ?: UiVenue(24,
                    "575fecea498e12354cfd4402",
                    "TestLocation",
                    "TestCat",
                    "1.11111,11,111",
                    "https://igx.4sqi.net/img/general/original/6036_Xv3VOJm0A8HMF8EbQWdKPXIce7LxcvXOMt4_nW5gDhU.jpg",
                    false,
                    0)

    override fun intents(): Observable<DetailActivityIntent> = Observable.merge(
            initInitialIntent(),
            openMapsPS.hide()
    )

    override fun render(state: DetailActivityState) {
        Timber.d("new detail state=$state")
        state.detailVenue?.apply { renderVenueDetail(this) }
        state.searchedVenue?.apply { renderSearchUi(this) }
//        sliderLayout.setVisibility(state.showSlides)
//        if (state.showSlides) {
//            renderSlides(state.slides)
//        }

        if (state.showError)
            Toast.makeText(applicationContext, state.errorStringId, Toast.LENGTH_SHORT).show()

        if (state.closeActivity)
            finish()

        loading.setVisibility(state.loading)

        if (state.showMap) {
            showMap(state.mapUrl)
        }
    }

    private fun initOpenMapFabClicks() {
        fab.clicks()
                .withLatestFrom(Observable.just(searchedVenue), BiFunction<Any, UiVenue, DetailActivityIntent.OpenMapClickedIntent> { _, venue ->
                    DetailActivityIntent.OpenMapClickedIntent(venue)
                })
                .subscribe(openMapsPS)

    }

    private fun initInitialIntent() = Observable.just(DetailActivityIntent.InitialIntent(searchedVenue)).cast(DetailActivityIntent::class.java)

    private fun renderSlides(slides: Map<String, String>) {
        slides.forEach {
            val slide = TextSliderView(this)
                    .description(it.value)
                    .image(it.key)
                    .setScaleType(BaseSliderView.ScaleType.Fit)

            slider.addSlider(slide)
        }

        slider.setDuration(3000)
    }

    private fun renderSearchUi(venue: UiVenue) {
//        coverPhoto.post {
//            val request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(venue.photo))
//                    .setResizeOptions(ResizeOptions(coverPhoto.width, coverPhoto.height))
//                    .build()
//            val controller = Fresco.newDraweeControllerBuilder()
//                    .setOldController(coverPhoto.controller)
//                    .setImageRequest(request)
//                    .build()
//            coverPhoto.controller = controller
//        }
        coverPhoto.setImageURI(venue.photo)
        supportActionBar?.title = venue.name
        category.text = venue.cat
        location.text = venue.location
    }

    private fun renderVenueDetail(venue: UiVenueDetail) {
        bestPhoto.setVisibility(venue.showBestPhoto)
//        bestPhoto.post {
//            val request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(venue.bestPhoto))
//                    .setResizeOptions(ResizeOptions(bestPhoto.width, bestPhoto.height))
//                    .build()
//            val controller = Fresco.newDraweeControllerBuilder()
//                    .setOldController(bestPhoto.controller)
//                    .setImageRequest(request)
//                    .build()
//            bestPhoto.controller = controller
//        }
        bestPhoto.setImageURI(venue.bestPhoto)
        category.text = venue.cat
        categoryIcon.setImageURI(venue.catIcon)
        address.text = venue.address
        phone.text = venue.phone
        city.text = venue.city
        rate.text = venue.rate

    }

    private fun showMap(mapUri: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(mapUri))
        startActivity(intent)
    }
}