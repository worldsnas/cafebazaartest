package com.worldsnas.cafebazaartest.activity.main.model.repo.network

import com.worldsnas.cafebazaartest.activity.main.model.repo.MainRepoKey
import com.worldsnas.cafebazaartest.app.network.retrofitutils.RetrofitRetryHandler
import com.worldsnas.cafebazaartest.app.network.retrofitutils.createErrorResponse
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.search.RecommendationResponseModel
import com.worldsnas.cafebazaartest.utils.*
import com.worldsnas.panther.Fetcher
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject

class MainActivityFetcher @Inject constructor(private val api: SearchApi) : Fetcher<MainRepoKey, Response<BaseModel<RecommendationResponseModel>>> {

    override fun fetch(key: MainRepoKey): Single<Response<BaseModel<RecommendationResponseModel>>> =
            api.explore(CLIENT_ID,
                    CLIENT_SECRET,
                    API_VERSION,
                    SEARCH_PHOTO,
                    SEARCH_LIMIT,
                    "${key.location.lat},${key.location.lng}",
                    key.offset)
                    .retry(RetrofitRetryHandler.createDefault())
                    .onErrorReturn {
                        createErrorResponse(it)
                    }

}