package com.worldsnas.cafebazaartest.activity.main.uimodels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UiVenue(
        val dbId : Long,
        val venueId : String,
        val name : String,
        val cat : String,
        val location : String,
        val photo : String,
        val verified : Boolean,
        val page : Int) : Parcelable