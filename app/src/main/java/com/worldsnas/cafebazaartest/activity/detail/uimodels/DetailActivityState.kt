package com.worldsnas.cafebazaartest.activity.detail.uimodels

import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.mvibase.MviViewState

data class DetailActivityState(
        val showSlides : Boolean,
        val slides : Map<String, String>,
        val searchedVenue : UiVenue?,
        val showMap : Boolean,
        val mapUrl : String,
        val closeActivity: Boolean,
        val detailVenue : UiVenueDetail?,
        val loading: Boolean,
        val showError: Boolean,
        val errorStringId : Int
) : MviViewState {
    companion object {
        @JvmStatic
        fun init() = DetailActivityState(
                false,
                emptyMap(),
                null,
                false,
                "",
                false,
                null,
                false,
                false,
                0)
    }
}