package com.worldsnas.cafebazaartest.activity.detail.presentation

import androidx.lifecycle.ViewModel
import com.worldsnas.cafebazaartest.activity.detail.model.DetailActivityProcessorHolder
import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailActivityRepoModel
import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailRepoKey
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailActivityDBModel
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailActivityDBValidator
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailActivityPersister
import com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper.DetailActivityDBRepoMapper
import com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper.DetailActivityServerDBMapper
import com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper.DetailVenueWithPhotoUiMapper
import com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper.VenueDetailResponseEntityMapper
import com.worldsnas.cafebazaartest.activity.detail.model.repo.network.DetailActivityFetcher
import com.worldsnas.cafebazaartest.activity.detail.model.repo.network.DetailApi
import com.worldsnas.cafebazaartest.activity.detail.processormodels.DetailActivityAction
import com.worldsnas.cafebazaartest.activity.detail.processormodels.DetailActivityResult
import com.worldsnas.cafebazaartest.activity.detail.uimodels.UiVenueDetail
import com.worldsnas.cafebazaartest.app.viewmodel.ViewModelKey
import com.worldsnas.cafebazaartest.base.BiMapper
import com.worldsnas.cafebazaartest.base.Mapper
import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto
import com.worldsnas.cafebazaartest.mvibase.MviProcessor
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailResponseModel
import com.worldsnas.cafebazaartest.utils.create
import com.worldsnas.panther.Panther
import com.worldsnas.panther.RealPanther
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Response
import retrofit2.Retrofit

@Module
abstract class DetailActivityViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(clazz = DetailActivityViewModel::class)
    abstract fun bindMainViewModel(viewModel: DetailActivityViewModel): ViewModel

    @Binds
    abstract fun bindProcessor(processor: DetailActivityProcessorHolder): MviProcessor<DetailActivityAction, DetailActivityResult>

    @Binds
    abstract fun bindVenueServerEntityMapper(mapper: VenueDetailResponseEntityMapper): Mapper<VenueDetailModel, DetailVenueWithPhoto>

    @Binds
    abstract fun bindDetailVenueWithPhotoUiMapper(mapper: DetailVenueWithPhotoUiMapper): BiMapper<DetailVenueWithPhoto, UiVenueDetail>

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun createRetrofitInstance(retrofit: Retrofit) = retrofit.create<DetailApi>()

        @JvmStatic
        @Provides
        fun provideMainActivityPanther(fetcher: DetailActivityFetcher,
                                       persister: DetailActivityPersister,
                                       serverMapper: DetailActivityServerDBMapper,
                                       dbMapper: DetailActivityDBRepoMapper,
                                       validator: DetailActivityDBValidator)
                : Panther<DetailActivityRepoModel, Response<BaseModel<VenueDetailResponseModel>>, DetailActivityDBModel, DetailRepoKey> =
                RealPanther(fetcher, persister, serverMapper, dbMapper, validator)
    }
}