package com.worldsnas.cafebazaartest.activity.detail.model.repo.db

import com.worldsnas.cafebazaartest.activity.detail.model.repo.DetailRepoKey
import com.worldsnas.panther.DBValidator
import javax.inject.Inject

class DetailActivityDBValidator @Inject constructor() : DBValidator<DetailRepoKey, DetailActivityDBModel> {
    override fun isEmpty(key: DetailRepoKey, dbModel: DetailActivityDBModel): Boolean =
            when (dbModel) {
                is DetailActivityDBModel.Success -> {
                    false
                }
                else -> {
                    true
                }
            }
}