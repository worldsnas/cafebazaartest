package com.worldsnas.cafebazaartest.activity.main.model.repo.mapper

import com.worldsnas.cafebazaartest.activity.main.model.repo.db.MainActivityDBModel
import com.worldsnas.cafebazaartest.app.network.retrofitutils.getStatusCodeErrorString
import com.worldsnas.cafebazaartest.base.ErrorHolder
import com.worldsnas.cafebazaartest.base.Mapper
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.search.RecommendationResponseModel
import com.worldsnas.cafebazaartest.servermodel.search.ResultModel
import com.worldsnas.panther.ServerMapper
import retrofit2.Response
import javax.inject.Inject

class MainActivityServerDBMapper @Inject constructor(
        private val groupItemEntityMapper: Mapper<ResultModel, SearchVenueEntity>
) : ServerMapper<Response<BaseModel<RecommendationResponseModel>>, MainActivityDBModel> {
    override fun map(serverModel: Response<BaseModel<RecommendationResponseModel>>): MainActivityDBModel =
            if (serverModel.isSuccessful) {
                val items = serverModel.body()?.data?.group?.results?.map { groupItemEntityMapper.map(it) }
                        ?: emptyList()
                MainActivityDBModel.Success(items)
            } else {
                MainActivityDBModel.Error(ErrorHolder.StringRes(getStatusCodeErrorString(serverModel.code())))
            }

}