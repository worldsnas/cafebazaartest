package com.worldsnas.cafebazaartest.activity.main.presentation

import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityAction
import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityResult
import com.worldsnas.cafebazaartest.activity.main.uimodels.MainActivityIntent
import com.worldsnas.cafebazaartest.activity.main.uimodels.MainActivityState
import com.worldsnas.cafebazaartest.base.BaseViewModel
import com.worldsnas.cafebazaartest.mvibase.MviProcessor
import com.worldsnas.cafebazaartest.utils.Load
import com.worldsnas.cafebazaartest.utils.locationDistance
import com.worldsnas.cafebazaartest.utils.notOfType
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
        private val processor: MviProcessor<MainActivityAction, MainActivityResult>
) : BaseViewModel<MainActivityIntent, MainActivityState>() {

    private val intentFilter = ObservableTransformer<MainActivityIntent, MainActivityIntent> { intents ->
        intents.publish { share ->
            Observable.merge(share.ofType(MainActivityIntent.InitialIntent::class.java).take(1),
                    share.notOfType(MainActivityIntent.InitialIntent::class.java)
            )
        }
    }

    private val openDetailDelay = ObservableTransformer<MainActivityAction, MainActivityAction> { action ->
        action.publish { share ->
            Observable.merge(
                    share.ofType(MainActivityAction.OpenDetailAction::class.java).throttleFirst(500, TimeUnit.MILLISECONDS),
                    share.notOfType(MainActivityAction.OpenDetailAction::class.java)
            )
        }
    }

    private val intentMapper = Function<MainActivityIntent, MainActivityAction> { intent ->
        when (intent) {
            is MainActivityIntent.InitialIntent -> MainActivityAction.Permission(intent.hasPermission, false)
            is MainActivityIntent.UserPermission -> MainActivityAction.Permission(intent.granted, true)
            is MainActivityIntent.LocationUpdate -> MainActivityAction.LoadLocations(intent.location, Load.initial())
            is MainActivityIntent.LoadMore -> MainActivityAction.LoadLocations(intent.location, intent.load)
            is MainActivityIntent.VenueClickIntent -> MainActivityAction.OpenDetailAction(intent.position)
        }
    }

    private val filterLocationUpdates = ObservableTransformer<MainActivityIntent, MainActivityIntent> { actions ->
        actions.publish { share ->
            Observable.merge(
                    share.ofType(MainActivityIntent.LocationUpdate::class.java).distinctUntilChanged { pre, new ->
                        val distance = locationDistance(pre.location.lat, new.location.lat, pre.location.lng, new.location.lng)
                        distance <= 100
                    },
                    share.notOfType(MainActivityIntent.LocationUpdate::class.java)
            )
        }
    }

    private val reducer = BiFunction<MainActivityState, MainActivityResult, MainActivityState> { preState, result ->
        when (result) {
            is MainActivityResult.LastStableResult -> {
                preState.copy(showError = false, loading = false, openDetail = false, closeActivity = false, requestPermission = false, permissionGranted = false)
            }
            is MainActivityResult.StartLocationUpdates -> {
                preState.copy(showError = false, loading = false, openDetail = false, closeActivity = false, requestPermission = false, permissionGranted = true)
            }
            is MainActivityResult.GetPermission -> {
                preState.copy(showError = false, loading = false, openDetail = false, closeActivity = false, requestPermission = true, permissionGranted = false)
            }
            is MainActivityResult.CloseActivity -> {
                preState.copy(showError = false, loading = false, openDetail = false, closeActivity = true, requestPermission = false, permissionGranted = false)
            }
            is MainActivityResult.Venues -> {
                preState.copy(showError = false, loading = false, openDetail = false, closeActivity = false, requestPermission = false, permissionGranted = false, venues = result.venues)
            }
            is MainActivityResult.Error -> {
                preState.copy(showError = true, loading = false, openDetail = false, closeActivity = false, requestPermission = false, permissionGranted = false, errorStringId = result.stringId)
            }
            is MainActivityResult.InFlight -> {
                preState.copy(showError = false, loading = true, openDetail = false, closeActivity = false, requestPermission = false, permissionGranted = false)
            }
            is MainActivityResult.OpenDetail -> {
                preState.copy(showError = false, loading = false, openDetail = true, closeActivity = false, requestPermission = false, permissionGranted = false, detailVenue = result.venue)
            }
        }
    }

    private val intentsSubject: PublishSubject<MainActivityIntent> = PublishSubject.create()
    private val statesObservable: Observable<MainActivityState> = compose()

    private fun compose(): Observable<MainActivityState> {
        return intentsSubject
//                .compose(intentFilter)
                .compose(filterLocationUpdates)
                .map(intentMapper)
                .compose(openDetailDelay)
                .compose(processor.actionProcessor)
                .scan(MainActivityState.init(), reducer)
                .doOnNext { Timber.d("new state created= $it") }
                .distinctUntilChanged()
                .replay(1)
                .autoConnect(0)
    }

    override fun states(): Observable<MainActivityState> = statesObservable

    override fun processIntents(intents: Observable<MainActivityIntent>) {
        intents.subscribe(intentsSubject)
    }
}