package com.worldsnas.cafebazaartest.activity.main.uimodels

import com.worldsnas.cafebazaartest.mvibase.MviViewState

data class MainActivityState(
        val venues: List<UiVenue>,
        val userLocation: String,
        val requestPermission: Boolean,
        val permissionGranted: Boolean,
        val closeActivity: Boolean,
        val openDetail : Boolean,
        val detailVenue : UiVenue?,
        val loading: Boolean,
        val showError: Boolean,
        val errorStringId : Int
) : MviViewState {
    companion object {
        @JvmStatic
        fun init() = MainActivityState(
                emptyList(),
                "",
                false,
                false,
                false,
                false,
                null,
                false,
                false,
                0)
    }
}