package com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper

import com.worldsnas.cafebazaartest.activity.detail.uimodels.UiVenueDetail
import com.worldsnas.cafebazaartest.base.BiMapper
import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto
import javax.inject.Inject

class DetailVenueWithPhotoUiMapper @Inject constructor() : BiMapper<DetailVenueWithPhoto, UiVenueDetail> {
    override fun mapBack(item: UiVenueDetail): DetailVenueWithPhoto {
        TODO("No Time") //To change body of created functions use File | Settings | File Templates.
    }

    override fun mapTo(item: DetailVenueWithPhoto): UiVenueDetail =
            UiVenueDetail(item.venue.venue_id,
                    item.venue.name,
                    item.venue.cat,
                    item.venue.phone,
                    item.venue.catIcon,
                    item.venue.address,
                    item.venue.city,
                    item.venue.rate,
                    item.venue.bestPhoto.isNotBlank(),
                    item.venue.bestPhoto)

}