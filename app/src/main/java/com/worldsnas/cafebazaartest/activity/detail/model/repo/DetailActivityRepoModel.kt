package com.worldsnas.cafebazaartest.activity.detail.model.repo

import com.worldsnas.cafebazaartest.activity.detail.uimodels.UiVenueDetail
import com.worldsnas.cafebazaartest.base.ErrorHolder

sealed class DetailActivityRepoModel {

    class Success(val venueDetail: UiVenueDetail, val photos: Map<String, String>) : DetailActivityRepoModel()
    class Error(val error: ErrorHolder) : DetailActivityRepoModel()
}