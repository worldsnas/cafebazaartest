package com.worldsnas.cafebazaartest.activity.detail.model.repo.mapper

import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.detail.model.repo.db.DetailActivityDBModel
import com.worldsnas.cafebazaartest.app.network.retrofitutils.getStatusCodeErrorString
import com.worldsnas.cafebazaartest.base.ErrorHolder
import com.worldsnas.cafebazaartest.base.Mapper
import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailModel
import com.worldsnas.cafebazaartest.servermodel.detail.VenueDetailResponseModel
import com.worldsnas.panther.ServerMapper
import retrofit2.Response
import javax.inject.Inject

class DetailActivityServerDBMapper @Inject constructor(
        private val mapper: Mapper<VenueDetailModel, DetailVenueWithPhoto>
) : ServerMapper<Response<BaseModel<VenueDetailResponseModel>>, DetailActivityDBModel> {
    override fun map(serverModel: Response<BaseModel<VenueDetailResponseModel>>): DetailActivityDBModel =
            if (serverModel.isSuccessful) {
                if (serverModel.body() == null || serverModel.body()!!.data == null) {
                    DetailActivityDBModel.Error(ErrorHolder.StringRes(R.string.error_no_item_received))
                }else{

                }
                DetailActivityDBModel.Success(mapper.map(serverModel.body()!!.data!!.venueDetailModel))
            } else {
                DetailActivityDBModel.Error(ErrorHolder.StringRes(getStatusCodeErrorString(serverModel.code())))
            }

}