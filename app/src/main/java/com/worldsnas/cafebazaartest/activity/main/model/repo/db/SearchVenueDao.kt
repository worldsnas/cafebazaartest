package com.worldsnas.cafebazaartest.activity.main.model.repo.db

import androidx.room.*
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single


@Dao
interface SearchVenueDao {
    @Query("SELECT * FROM search_venue WHERE cache_time >= :yesterday AND page = :page")
    fun getNotStale(yesterday: Long, page : Int) : Observable<List<SearchVenueEntity>>

    @Query("SELECT * FROM search_venue WHERE cache_time >= :yesterday AND page = :page ")
    fun getNotStaleSingle(yesterday: Long, page : Int) : Single<List<SearchVenueEntity>>

    @Query("SELECT * FROM search_venue WHERE cache_time < :yesterday")
    fun getStale(yesterday: Long) : Observable<List<SearchVenueEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(venues: List<SearchVenueEntity>) : Completable

    @Delete
    fun delete(venue: SearchVenueEntity)

    @Delete
    fun deleteAll(venues : List<SearchVenueEntity>)
}
