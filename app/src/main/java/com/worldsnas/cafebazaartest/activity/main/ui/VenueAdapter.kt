package com.worldsnas.cafebazaartest.activity.main.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.utils.inflate
import com.worldsnas.cafebazaartest.viewholder.venue.VenueViewHolder
import com.worldsnas.cafebazaartest.viewholder.venue.VenueViewHolderIntent
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class VenueAdapter @Inject constructor(diffUtil : VenueDiffCallback): ListAdapter<UiVenue, VenueViewHolder>(diffUtil){

    private val clickPs = PublishSubject.create<VenueViewHolderIntent>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueViewHolder =
            VenueViewHolder(parent.inflate(R.layout.row_venue))

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemOnClick(clickPs)
    }

    fun getClicks() = clickPs.hide()
}