package com.worldsnas.cafebazaartest.activity.main.model.repo.mapper

import com.worldsnas.cafebazaartest.base.Mapper
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity
import com.worldsnas.cafebazaartest.servermodel.search.CategoryModel
import com.worldsnas.cafebazaartest.servermodel.search.PhotoModel
import com.worldsnas.cafebazaartest.servermodel.search.ResultModel
import javax.inject.Inject

class GroupItemEntityMapper @Inject constructor() : Mapper<ResultModel, SearchVenueEntity> {

    override fun map(item: ResultModel): SearchVenueEntity =
            SearchVenueEntity(0,
                    item.venue.id,
                    item.venue.name,
                    getPrimaryCategory(item.venue.categories),
                    getPhoto(item.photo),
                    "${item.venue.location.lat},${item.venue.location.lng}",
                    item.venue.verified)


    private fun getPrimaryCategory(categories: List<CategoryModel>?): String {
        if (categories == null || categories.isEmpty())
            return ""
        return categories.find { it.primary?: false }?.name ?: categories[0].name ?: ""
    }

    private fun getPhoto(photo: PhotoModel?): String =
            photo?.let {
                it.prefix + "original" + it.suffix
            } ?: ""


}