package com.worldsnas.cafebazaartest.activity.main.di

import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.worldsnas.cafebazaartest.activity.main.ui.MainActivity
import com.worldsnas.cafebazaartest.location.FusedLocationProvider
import com.worldsnas.cafebazaartest.location.LocationProvider
import dagger.Module
import dagger.Provides

@Module
object MainActivityModule {

    @JvmStatic
    @Provides
    fun provideFusedLocationProviderClient(context: MainActivity): FusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(context)

    @JvmStatic
    @Provides
    fun provideLocationProvider(activity : MainActivity, fused : FusedLocationProviderClient) : LocationProvider =
        FusedLocationProvider(activity, fused)
}