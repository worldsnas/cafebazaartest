package com.worldsnas.cafebazaartest.activity.main.model.repo.db

import com.worldsnas.cafebazaartest.base.ErrorHolder
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity

sealed class MainActivityDBModel {

    class Success(val venues : List<SearchVenueEntity>) : MainActivityDBModel()
    class Error(val error : ErrorHolder) : MainActivityDBModel()
}