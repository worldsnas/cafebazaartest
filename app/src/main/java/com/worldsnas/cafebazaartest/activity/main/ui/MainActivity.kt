package com.worldsnas.cafebazaartest.activity.main.ui

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.worldsnas.cafebazaartest.R
import com.worldsnas.cafebazaartest.activity.detail.ui.DetailActivity
import com.worldsnas.cafebazaartest.activity.main.presentation.MainActivityViewModel
import com.worldsnas.cafebazaartest.activity.main.uimodels.LatLang
import com.worldsnas.cafebazaartest.activity.main.uimodels.MainActivityIntent
import com.worldsnas.cafebazaartest.activity.main.uimodels.MainActivityState
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.base.BaseActivity
import com.worldsnas.cafebazaartest.base.createViewModel
import com.worldsnas.cafebazaartest.location.LocationProvider
import com.worldsnas.cafebazaartest.utils.EndlessRecyclerViewScrollListener
import com.worldsnas.cafebazaartest.utils.Load
import com.worldsnas.cafebazaartest.utils.SimpleItemDecorator
import com.worldsnas.cafebazaartest.utils.roundDecimal
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Provider

class MainActivity : BaseActivity<MainActivityIntent, MainActivityState, MainActivityViewModel>() {
    @Inject
    lateinit var locationProvider: LocationProvider
    @Inject
    lateinit var adapterProvider: Provider<VenueAdapter>
    lateinit var adapter: VenueAdapter
    lateinit var layoutManager: LinearLayoutManager

    private val loadMorePs = PublishSubject.create<Load>()
    //from user
    private val permissionUserActionSubject = PublishSubject.create<Boolean>()
    private val locationUpdatePS = PublishSubject.create<LatLang>()

    private val locationCallback: (latitude: Double, longitude: Double) -> Unit = { latitude, longitude ->
        locationUpdatePS.onNext(LatLang.create(latitude.roundDecimal(4), longitude.roundDecimal(4)))
    }

    private val permissions: Array<out String> = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    )
    private val LOCATION_REQUEST_PERMISSION_STATUS_CODE = 1005

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createViewModel()
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(Color.WHITE)

        initRv()
    }

    override fun onStop() {
        super.onStop()
        locationProvider.cancelUpdates(locationCallback)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_REQUEST_PERMISSION_STATUS_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted for both requests
                permissionUserActionSubject.onNext(true)
            } else {
                //permission denied for both requests
                permissionUserActionSubject.onNext(false)
            }
        }
    }

    private fun registerLocationCallback() {
        locationProvider.requestUpdates(locationCallback)
    }

    private fun initRv() {
        val offset = resources.getDimension(R.dimen.sml_ui_normal_size).toInt()
        layoutManager = LinearLayoutManager(this)
        adapter = adapterProvider.get()
        recycler.adapter = adapter
        recycler.layoutManager = layoutManager
        recycler.addItemDecoration(SimpleItemDecorator(top = offset, left = offset, right = offset))
    }

    private fun initLocationUpdates() = locationUpdatePS
            .hide()
            .map {
                MainActivityIntent.LocationUpdate(it)
            }

    private fun initVenueClicks() = adapter.getClicks().map { MainActivityIntent.VenueClickIntent(it.adapterPosition) }

    private fun initInitialIntent() = Observable.just(MainActivityIntent.InitialIntent(
            permissions.all { ActivityCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED }))

    private fun initLoadMoreIntent(): Observable<MainActivityIntent> {
        recycler.addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                loadMorePs.onNext(Load.create(page, totalItemsCount))
            }
        })
        return loadMorePs
                .hide()
                .withLatestFrom(locationUpdatePS, BiFunction<Load, LatLang, MainActivityIntent> { load: Load, latLang: LatLang ->
                    MainActivityIntent.LoadMore(latLang, load)
                })
                .doOnNext {
                    Timber.d("load more received= $it")
                }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, permissions, LOCATION_REQUEST_PERMISSION_STATUS_CODE)
    }

    private fun initUserPermission() = permissionUserActionSubject
            .map {
                MainActivityIntent.UserPermission(it)
            }
            .hide()

    override fun intents(): Observable<MainActivityIntent> =
            Observable.merge(initInitialIntent(),
                    initVenueClicks(),
                    initLoadMoreIntent(),
                    initLocationUpdates())
                    .mergeWith(initUserPermission())

    override fun render(state: MainActivityState) {
        adapter.submitList(state.venues)
        userLocation.text = state.userLocation
        if (state.permissionGranted) {
            registerLocationCallback()
        }
        if (state.requestPermission) {
            requestPermission()
        }
        if (state.closeActivity) {
            finish()
        }
        if (state.showError) {
            Toast.makeText(this, state.errorStringId, Toast.LENGTH_SHORT).show()
        }
        if (state.openDetail) {
            openDetail(state.detailVenue)
        }
    }

    private fun openDetail(venue: UiVenue?) {
        venue?.let {
            startActivity(DetailActivity.createIntent(this, it))
        }
    }
}