package com.worldsnas.cafebazaartest.activity.main.model

import com.worldsnas.cafebazaartest.activity.main.model.repo.MainActivityRepoModel
import com.worldsnas.cafebazaartest.activity.main.model.repo.MainRepoKey
import com.worldsnas.cafebazaartest.activity.main.model.repo.db.MainActivityDBModel
import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityAction
import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityResult
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.base.getStringRes
import com.worldsnas.cafebazaartest.mvibase.MviProcessor
import com.worldsnas.cafebazaartest.rxutils.SchedulersFacade
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.search.RecommendationResponseModel
import com.worldsnas.cafebazaartest.utils.Load
import com.worldsnas.panther.Panther
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.Function3
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.properties.Delegates

class MainActivityProcessorHolder @Inject constructor(
        private val repo: Panther<MainActivityRepoModel, Response<BaseModel<RecommendationResponseModel>>, MainActivityDBModel, MainRepoKey>,
        private val schedulersFacade: SchedulersFacade
) : MviProcessor<MainActivityAction, MainActivityResult> {

    private val venueChangePs = BehaviorSubject.create<List<UiVenue>>().also {
        it.onNext(emptyList())
    }

    var venues by Delegates.observable(emptyList<UiVenue>()) { _, old, new ->
        venueChangePs.onNext(new)
    }
//            emptyList<UiVenue>()

    override val actionProcessor: ObservableTransformer<MainActivityAction, MainActivityResult>
        get() = ObservableTransformer { actions ->
            actions.publish { share ->
                Observable.merge(
                        share.ofType(MainActivityAction.Permission::class.java).compose(permissionProcessor),
                        share.ofType(MainActivityAction.LoadLocations::class.java).compose(locationProcessor),
                        share.ofType(MainActivityAction.OpenDetailAction::class.java).compose(openDetailProcessor)
                )
            }
        }

    private val permissionProcessor = ObservableTransformer<MainActivityAction.Permission, MainActivityResult> { actions ->
        actions.publish { share ->
            Observable.merge(
                    //we have permission so we can start location updates
                    share.filter { it.hasPermission }.map { MainActivityResult.StartLocationUpdates },
                    //we don't have permission, we need to ask
                    share.filter { !it.hasPermission && !it.userProvided }.map { MainActivityResult.GetPermission },
                    //user did not provide permission
                    share.filter { !it.hasPermission && it.userProvided }.map { MainActivityResult.CloseActivity }
            )
        }
    }

    private val locationProcessor = ObservableTransformer<MainActivityAction.LoadLocations, MainActivityResult> { actions ->
        actions
                .map { result ->
                    if (venues.isEmpty() || result.load.page == 0) {
                        result
                    } else {
                        MainActivityAction.LoadLocations(result.location, Load.create(venues.size, venues.size))
                    }
                }
                .publish { share ->
                    share
                            .switchMap {
                                val key = MainRepoKey.create(it.location, it.load.page)
                                repo.get(key)
                                        .subscribeOn(schedulersFacade.io())
                                        .toObservable()
                            }
                            .map {
                                when (it) {
                                    is MainActivityRepoModel.Success -> MainActivityResult.Venues(it.venues)
                                    is MainActivityRepoModel.Error -> MainActivityResult.Error(it.error.getStringRes())
                                }
                            }
                            .withLatestFrom(share, venueChangePs, Function3<MainActivityResult, MainActivityAction.LoadLocations, List<UiVenue>, MainActivityResult>
                            { result, action, venues ->
                                when (result) {
                                    is MainActivityResult.Venues -> {
                                        if (action.load.page == 0) {
                                            result
                                        } else {
                                            val newVenues = ArrayList(venues)
                                            newVenues.addAll(result.venues)
                                            MainActivityResult.Venues(newVenues)
                                        }
                                    }
                                    else -> result
                                }

                            })
                            .flatMap {
                                when (it) {
                                    is MainActivityResult.Error -> {
                                        Observable.just(MainActivityResult.LastStableResult)
                                                .cast(MainActivityResult::class.java)
                                                .delay(500, TimeUnit.MILLISECONDS)
                                                .startWith(it)
                                    }
                                    else -> {
                                        Observable.just(it)
                                    }
                                }
                            }
                            .observeOn(schedulersFacade.ui())
                            .startWith(MainActivityResult.InFlight)
                            .doOnNext {
                                if (it is MainActivityResult.Venues) {
                                    venues = it.venues
                                }
                            }
                }
    }

    private val openDetailProcessor = ObservableTransformer<MainActivityAction.OpenDetailAction, MainActivityResult> { actions ->
        actions
                .switchMap {
                    if (venues.size > it.position) {
                        Observable.just(MainActivityResult.LastStableResult)
                                .cast(MainActivityResult::class.java)
                                .delay(200, TimeUnit.MILLISECONDS)
                                .startWith(MainActivityResult.OpenDetail(venues[it.position]))
                    } else {
                        Observable.just(MainActivityResult.LastStableResult)
                    }
                }
                .observeOn(schedulersFacade.ui())
    }

}