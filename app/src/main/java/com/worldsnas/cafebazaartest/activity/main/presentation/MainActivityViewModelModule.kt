package com.worldsnas.cafebazaartest.activity.main.presentation

import androidx.lifecycle.ViewModel
import com.worldsnas.cafebazaartest.activity.main.model.MainActivityProcessorHolder
import com.worldsnas.cafebazaartest.activity.main.model.repo.MainActivityRepoModel
import com.worldsnas.cafebazaartest.activity.main.model.repo.MainRepoKey
import com.worldsnas.cafebazaartest.activity.main.model.repo.db.MainActivityDBModel
import com.worldsnas.cafebazaartest.activity.main.model.repo.db.MainActivityDBValidator
import com.worldsnas.cafebazaartest.activity.main.model.repo.db.MainActivityPersister
import com.worldsnas.cafebazaartest.activity.main.model.repo.mapper.GroupItemEntityMapper
import com.worldsnas.cafebazaartest.activity.main.model.repo.mapper.MainActivityDBRepoMapper
import com.worldsnas.cafebazaartest.activity.main.model.repo.mapper.MainActivityServerDBMapper
import com.worldsnas.cafebazaartest.activity.main.model.repo.mapper.SearchVenueEntityUIMapper
import com.worldsnas.cafebazaartest.activity.main.model.repo.network.MainActivityFetcher
import com.worldsnas.cafebazaartest.activity.main.model.repo.network.SearchApi
import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityAction
import com.worldsnas.cafebazaartest.activity.main.processormodels.MainActivityResult
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.app.viewmodel.ViewModelKey
import com.worldsnas.cafebazaartest.base.BiMapper
import com.worldsnas.cafebazaartest.base.Mapper
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity
import com.worldsnas.cafebazaartest.mvibase.MviProcessor
import com.worldsnas.cafebazaartest.servermodel.BaseModel
import com.worldsnas.cafebazaartest.servermodel.search.*
import com.worldsnas.cafebazaartest.utils.create
import com.worldsnas.panther.Panther
import com.worldsnas.panther.RealPanther
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Response
import retrofit2.Retrofit

@Module
abstract class MainActivityViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(clazz = MainActivityViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainActivityViewModel): ViewModel

    @Binds
    abstract fun bindProcessor(processor: MainActivityProcessorHolder): MviProcessor<MainActivityAction, MainActivityResult>

    @Binds
    abstract fun bindServerMapper(mapper: GroupItemEntityMapper): Mapper<ResultModel, SearchVenueEntity>

    @Binds
    abstract fun bindUIMapper(mapper: SearchVenueEntityUIMapper): BiMapper<SearchVenueEntity, UiVenue>

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun createRetrofitInstance(retrofit: Retrofit) = retrofit.create<SearchApi>()

        @JvmStatic
        @Provides
        fun provideMainActivityPanther(fetcher: MainActivityFetcher,
                                       persister: MainActivityPersister,
                                       serverMapper: MainActivityServerDBMapper,
                                       dbMapper: MainActivityDBRepoMapper,
                                       validator: MainActivityDBValidator)
                : Panther<MainActivityRepoModel, Response<BaseModel<RecommendationResponseModel>>, MainActivityDBModel, MainRepoKey> =
                RealPanther(fetcher, persister, serverMapper, dbMapper, validator)
    }
}