package com.worldsnas.cafebazaartest.activity.detail.model.repo.db

import com.worldsnas.cafebazaartest.base.ErrorHolder
import com.worldsnas.cafebazaartest.entity.DetailVenueWithPhoto

sealed class DetailActivityDBModel {

    class Success(val venue : DetailVenueWithPhoto) : DetailActivityDBModel()
    class Error(val error : ErrorHolder) : DetailActivityDBModel()
}