package com.worldsnas.cafebazaartest.activity.main.model.repo.mapper

import com.worldsnas.cafebazaartest.activity.main.model.repo.MainActivityRepoModel
import com.worldsnas.cafebazaartest.activity.main.model.repo.db.MainActivityDBModel
import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.base.BiMapper
import com.worldsnas.cafebazaartest.entity.SearchVenueEntity
import com.worldsnas.panther.DBMapper
import javax.inject.Inject

class MainActivityDBRepoMapper @Inject constructor(
        private val entityUIMapper : BiMapper<SearchVenueEntity, UiVenue>
) : DBMapper<MainActivityDBModel, MainActivityRepoModel> {

    override fun mapDb(dbModel: MainActivityDBModel): MainActivityRepoModel =
            when (dbModel) {
                is MainActivityDBModel.Success -> {
                    MainActivityRepoModel.Success(dbModel.venues.map { entityUIMapper.mapTo(it) })
                }
                is MainActivityDBModel.Error -> {
                    MainActivityRepoModel.Error(dbModel.error)
                }
            }

    override fun mapRepo(repoModel: MainActivityRepoModel): MainActivityDBModel =
            when (repoModel) {
                is MainActivityRepoModel.Success -> {
                    MainActivityDBModel.Success(repoModel.venues.map { entityUIMapper.mapBack(it) })
                }
                is MainActivityRepoModel.Error -> {
                    MainActivityDBModel.Error(repoModel.error)
                }
            }


}