package com.worldsnas.cafebazaartest.activity.detail.uimodels

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UiVenueDetail(
        val venueId : String,
        val name : String,
        val cat : String,
        val phone: String,
        val catIcon : String,
        val address: String,
        val city : String,
        val rate : String,
        val showBestPhoto : Boolean,
        val bestPhoto : String) : Parcelable