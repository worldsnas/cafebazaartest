package com.worldsnas.cafebazaartest.activity.detail.uimodels

import com.worldsnas.cafebazaartest.activity.main.uimodels.UiVenue
import com.worldsnas.cafebazaartest.mvibase.MviIntent

sealed class DetailActivityIntent : MviIntent {

    class OpenMapClickedIntent(val venue: UiVenue) : DetailActivityIntent()
    class InitialIntent(val venue: UiVenue) : DetailActivityIntent()
}